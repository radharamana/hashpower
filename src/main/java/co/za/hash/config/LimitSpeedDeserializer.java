package co.za.hash.config;

import java.io.IOException;
import java.math.BigDecimal;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class LimitSpeedDeserializer extends StdDeserializer<BigDecimal> {

	private static final long serialVersionUID = 1L;

	public LimitSpeedDeserializer() { 
        this(null); 
    } 
	
	public  LimitSpeedDeserializer(Class<?> vc) {
		super(vc);
	}

	@Override
	public BigDecimal deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		JsonNode node = jp.getCodec().readTree(jp);
        
        return new BigDecimal(1000000).multiply(node.decimalValue());
	}

}
