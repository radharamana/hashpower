package co.za.hash.config;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JacksonAppProp {
	@JsonProperty("application")
	private ApplicationProperties props;

	public ApplicationProperties getProps() {
		return props;
	}

	public void setProps(ApplicationProperties props) {
		this.props = props;
	}
	
}
