package co.za.hash.config;

import static co.za.hash.util.LamdaUtil.entriesToMap;
import static co.za.hash.util.LamdaUtil.entry;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonRootName;

import co.za.hash.domain.DPool;
import co.za.hash.domain.Location;
import co.za.hash.domain.PoolFormat;

@Component
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
@JsonRootName("application")
public class ApplicationProperties {
	private List<DPool> dpools;
	//private List<PoolProp> pool;// = new Pool();
	private NiceHash nice;// = new NiceHash();
	private BigDecimal speedTol;
	private BigDecimal priceTol;
	private BigDecimal profitPercent;
	private BigDecimal rejectPercent;
	private List<MultiPool> multiPools;
	private Map<String, MultiPool> multiPoolMap;
	private List<Route> routes;
	private Map<String, Route> routeMap;
	private String email;
	
	public static class Route {
		private String name;
		private String timer;
		private String url;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getTimer() {
			return timer;
		}
		public void setTimer(String timer) {
			this.timer = timer;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
	}
	
	public static class MultiPool implements Comparable<MultiPool>{
		private String api;
		private String name;
		private List<Location> regions;
		private PoolFormat format;
		private BigDecimal fee = BigDecimal.ZERO;
		private String pool;
		
		public String getApi() {
			return api;
		}
		public void setApi(String api) {
			this.api = api;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public List<Location> getRegions() {
			return regions;
		}
		public void setRegions(List<Location> regions) {
			this.regions = regions;
		}
		public PoolFormat getFormat() {
			return format;
		}
		public void setFormat(PoolFormat format) {
			this.format = format;
		}
		@Override
		public int compareTo(MultiPool o) {
			return o.getName().compareTo(this.getName());
		}
		@Override
		public String toString() {
			return name;
		}
		public BigDecimal getFee() {
			return fee;
		}
		public void setFee(BigDecimal fee) {
			this.fee = fee;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof MultiPool))
				return false;
			MultiPool other = (MultiPool) obj;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}
		public String getPool() {
			return pool;
		}
		public void setPool(String pool) {
			this.pool = pool;
		}
		
		
		
		
	}
	
	public static class Algo implements Comparable<Algo>{
		private int id;
		private BigDecimal minSpeed;
		private BigDecimal adj = BigDecimal.ZERO;
		private int wtmId;
		private BigDecimal minDiff;
		private BigDecimal amount;
		private BigDecimal maxPrice;
		private List<String> names;
		
		public Algo(){}
		
		public Algo(int id){
			this.id = id;
		}

		public BigDecimal getMinSpeed() {
			return minSpeed;
		}

		public void setMinSpeed(BigDecimal minSpeed) {
			this.minSpeed = minSpeed;
		}

		public Integer getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		

		
		public BigDecimal getAdj() {
			return adj;
		}

		public void setAdj(BigDecimal adj) {
			this.adj = adj;
		}

		public int getWtmId() {
			return wtmId;
		}

		public void setWtmId(int wtmId) {
			this.wtmId = wtmId;
		}

		public List<String> getNames() {
			return names;
		}

		public void setNames(List<String> names) {
			this.names = names;
		}

//		@Override
//		public String toString() {
//			return "Algo [id=" + id + ", minSpeed=" + minSpeed + ", adj=" + adj + ", wtmId=" + wtmId + ", names="
//					+ names + "]";
//		}

		@Override
		public int compareTo(Algo o) {
			return this.getId().compareTo(o.getId());
		}
		
		@Override
		public String toString(){
			return names.get(0);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + id;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof Algo))
				return false;
			Algo other = (Algo) obj;
			if (id != other.id)
				return false;
			return true;
		}

		public BigDecimal getMinDiff() {
			return minDiff;
		}

		public void setMinDiff(BigDecimal minDiff) {
			this.minDiff = minDiff;
		}

		public BigDecimal getAmount() {
			return amount;
		}

		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}

		public BigDecimal getMaxPrice() {
			return maxPrice;
		}

		public void setMaxPrice(BigDecimal maxPrice) {
			this.maxPrice = maxPrice;
		}
		
		
		

	}

	public static class NiceHash {
		private Order order;
		private int apiId;
		private String apiKey;
		private List<Algo> algos;
		private String authKey;
		private String wallet;
		private BigDecimal fee;
		private BigDecimal decAmount;
		private Map<String, Algo> algoMap;
		
		
		

		public static class Order {
			private BigDecimal min;
			private BigDecimal refill;

			public BigDecimal getMin() {
				return min;
			}

			public void setMin(BigDecimal min) {
				this.min = min;
			}

			public BigDecimal getRefill() {
				return refill;
			}

			public void setRefill(BigDecimal refill) {
				this.refill = refill;
			}
		}

		public int getApiId() {
			return apiId;
		}

		public void setApiId(int apiId) {
			this.apiId = apiId;
		}

		public String getApiKey() {
			return apiKey;
		}

		public void setApiKey(String apiKey) {
			this.apiKey = apiKey;
		}

		
		public Order getOrder() {
			return order;
		}

		public void setOrder(Order order) {
			this.order = order;
		}

		
		public String getAuthKey() {
			return authKey;
		}

		public void setAuthKey(String authKey) {
			this.authKey = authKey;
		}

		public BigDecimal getFee() {
			return fee;
		}
		
		public void setFee(BigDecimal fee) {
			this.fee = fee;
		}

		public BigDecimal getDecAmount() {
			return decAmount;
		}

		public void setDecAmount(BigDecimal decAmount) {
			this.decAmount = decAmount;
		}
		
		
		public List<Algo> getAlgos() {
			return algos;
		}

		public void setAlgos(List<Algo> algos) {
			this.algos = algos;
		}
		
		public Map<String, Algo> getAlgoMap(){
			if(algoMap == null)algoMap = algos.stream().collect(Collectors.<Algo, String, Algo>toMap(a -> a.getNames().get(0), (a)->a));
			return this.algoMap;
		}

		public String getWallet() {
			return wallet;
		}

		public void setWallet(String wallet) {
			this.wallet = wallet;
		}
		

	}

	public List<DPool> getDpools() {
		return dpools;
	}

	public void setDpools(List<DPool> pools) {
		this.dpools = pools;
	}

	// public Pool getPool() {
	// return pool;
	// }
	//
	// public void setPool(Pool pool) {
	// this.pool = pool;
	// }

	public NiceHash getNice() {
		return nice;
	}

	public void setNice(NiceHash nice) {
		this.nice = nice;
	}

	public BigDecimal getSpeedTol() {
		return speedTol;
	}

	public void setSpeedTol(BigDecimal speedTol) {
		this.speedTol = speedTol;
	}

	public BigDecimal getPriceTol() {
		return priceTol;
	}

	public void setPriceTol(BigDecimal priceTol) {
		this.priceTol = priceTol;
	}

	public List<MultiPool> getMultiPools() {
		return multiPools;
	}

	public void setMultiPools(List<MultiPool> multiPools) {
		this.multiPools = multiPools;
	}
	
	public Map<String, MultiPool> getMultiPoolMap() {
		if(multiPoolMap == null){
			multiPoolMap = multiPools.stream().collect(Collectors.<MultiPool, String, MultiPool>toMap(MultiPool::getName, m->m));
		}
		return multiPoolMap;
	}

	public List<Route> getRoutes() {
		return routes;
	}

	public void setRoutes(List<Route> routes) {
		this.routes = routes;
		
	}

	public Map<String, Route> getRouteMap() {
		if(routeMap == null)routeMap = routes.stream().collect(Collectors.toMap(Route::getName, Function.identity()));
		return routeMap;
	}

	public BigDecimal getProfitPercent() {
		return profitPercent;
	}

	public void setProfitPercent(BigDecimal profitPercent) {
		this.profitPercent = profitPercent;
	}

	public BigDecimal getRejectPercent() {
		return rejectPercent;
	}

	public void setRejectPercent(BigDecimal rejectPercent) {
		this.rejectPercent = rejectPercent;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	

}