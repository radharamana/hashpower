package co.za.hash;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.camel.CamelContext;
import org.apache.camel.spring.boot.CamelContextConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class HashPowerApplication {

	public static void main(String[] args) {
		SpringApplication.run(HashPowerApplication.class, args);
	}
	
	@Bean
	CamelContextConfiguration contextConfiguration() {
	   return new CamelContextConfiguration() {
			@Override
			public void beforeApplicationStart(CamelContext context) {}

			@Override
			public void afterApplicationStart(CamelContext context) {
				
			}
	    };
	    
	   
	}
	
    
}
