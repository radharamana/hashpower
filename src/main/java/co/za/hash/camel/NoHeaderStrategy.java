package co.za.hash.camel;

import java.util.Arrays;
import java.util.HashSet;

import org.apache.camel.impl.DefaultHeaderFilterStrategy;
import org.springframework.stereotype.Component;

@Component
public class NoHeaderStrategy extends DefaultHeaderFilterStrategy {
	public NoHeaderStrategy(){
		//super.setInFilter(new HashSet<>(Arrays.asList("xnonex")));
		super.setOutFilterPattern(".*");
	}
}
