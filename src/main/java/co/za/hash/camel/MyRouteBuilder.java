package co.za.hash.camel;

import org.apache.camel.builder.RouteBuilder;

public abstract class MyRouteBuilder extends RouteBuilder {
	protected final String routeId;
	
	protected MyRouteBuilder(String routeId){
		this.routeId = routeId;
	}

	public String getRouteId() {
		return routeId;
	}
	
	

}
