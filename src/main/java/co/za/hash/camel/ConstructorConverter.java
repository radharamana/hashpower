package co.za.hash.camel;

import java.lang.reflect.Constructor;

import org.apache.camel.Converter;
import org.apache.camel.FallbackConverter;
import org.apache.camel.spi.TypeConverterRegistry;

/**
 * Convert to a type if there is a one argument constructor in that type that
 * can take the value as argument.
 */
@Converter
public class ConstructorConverter {
    @SuppressWarnings("unchecked")
    @FallbackConverter
    public static <T> T convertTo(Class<T> type, Object value, TypeConverterRegistry registry) throws Throwable {
        for (Constructor<?> c : type.getConstructors()) {
            Class<?>[] types = c.getParameterTypes();
            if (types.length == 1 && types[0].isAssignableFrom(value.getClass())) {
                    return (T) c.newInstance(value);
            }
        }
        return null;
    }
}