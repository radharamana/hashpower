package co.za.hash.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.properties.PropertiesComponent;
import org.springframework.stereotype.Component;

@Component
public class PropertyProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		PropertiesComponent properties = (PropertiesComponent)exchange.getContext().getComponent("properties");
		
		properties.getOverrideProperties().keySet().stream().filter(p->p.toString().contains("application."))
			.forEach(p->exchange.setProperty(p.toString().substring(12) , properties.getOverrideProperties().getProperty(p.toString())));
		
	}

}
