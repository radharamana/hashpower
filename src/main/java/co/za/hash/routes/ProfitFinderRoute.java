package co.za.hash.routes;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

import co.za.hash.config.ApplicationProperties;
import co.za.hash.domain.PoolFormat;
import co.za.hash.domain.mpool.YimpStatus;
import co.za.hash.domain.mpool.hub.HubStatus;
import co.za.hash.domain.nice.global.Stat;
import co.za.hash.service.MarketService;
import co.za.hash.service.MultiPoolService;

@Component
public class ProfitFinderRoute extends RouteBuilder {

	private static final String ROUTE_POOL_PRICES = "PoolPrices";
	private static final String ROUTE_NICE_STATS = "NiceStats";
	private static final String URL_NICE_STAT = "https://api.nicehash.com/api?method=stats.global.current";
	final ApplicationProperties props;
	final MarketService marketService;
	final MultiPoolService multiPoolService;
	
	
	public ProfitFinderRoute(ApplicationProperties props, MarketService market, MultiPoolService multiPoolService){
		this.props = props;
		this.marketService = market;
		this.multiPoolService = multiPoolService;
	}
	
	
			
	@Override
	public void configure() throws Exception {
		from(props.getRouteMap().get(ROUTE_NICE_STATS).getTimer())
			.log("Getting NiceHash price stats")
			.routeId(ROUTE_NICE_STATS)
			.to(URL_NICE_STAT)
			.unmarshal().json(JsonLibrary.Jackson, Stat.class)
			.bean(multiPoolService, MultiPoolService.M_DO_NICE_PRICE_UPDATE_nice)
			;
		
		from(props.getRouteMap().get(ROUTE_POOL_PRICES).getTimer())
			.routeId(ROUTE_POOL_PRICES)
			.loopDoWhile().method(multiPoolService, MultiPoolService.M_HAS_NEXT)
				.setHeader("pool").method(multiPoolService, MultiPoolService.M_NEXT)
				.choice()
					.when().simple("${header.pool.format} == "+PoolFormat.YIIMP.getCamelType())
						.log("Getting prices from ${header.pool.name}")
						.setHeader(Exchange.HTTP_METHOD).constant("GET").setBody().constant(null)
						.toD("${header.pool.api}?headerFilterStrategy=noHeaderStrategy")
						.transform().method(YimpStatus.class, YimpStatus.M_DO_WRAP)
						.unmarshal().json(JsonLibrary.Jackson, YimpStatus.class)
						.bean(multiPoolService, MultiPoolService.M_DO_PRICE_UPDATE_pool_yimp)
					.when().simple("${header.pool.format} == "+PoolFormat.HUB.getCamelType())
						.log("Getting prices from ${header.pool.name}")
						.setHeader(Exchange.HTTP_METHOD).constant("GET").setBody().constant(null)
						.toD("${header.pool.api}&headerFilterStrategy=noHeaderStrategy")
						.unmarshal().json(JsonLibrary.Jackson, HubStatus.class)
						.bean(multiPoolService, MultiPoolService.M_DO_PRICE_UPDATE_pool_hub)
					.otherwise().endChoice().end()
			.end()
			.bean(marketService, MarketService.M_UPDATE_MAX_PRICE)
			.setBody().method(multiPoolService, MultiPoolService.M_TO_STRING).log("${body}");
	}
	
//	private static final String M_HUB_API = "getHubApi";
//	public String getHubApi(){
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm");
//		String date = sdf.format(new Date());
//		return String.format(props.getMultiPoolMap().get("miningpoolhub").getApi(), date);
//	}

}
