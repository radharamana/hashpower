package co.za.hash.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spi.Registry;
import org.apache.camel.util.jsse.KeyStoreParameters;
import org.apache.camel.util.jsse.SSLContextParameters;
import org.apache.camel.util.jsse.TrustManagersParameters;
import org.springframework.stereotype.Component;

import co.za.hash.config.ApplicationProperties;

@Component
public class EmailRoute extends RouteBuilder {
	public static final String ID_BITCOIN_EMAIL = "BitcoinEmail";
	public static final String ID_BASIC_EMAIL = "BasicEmail";
	public static final String ROUTE_BASIC = "direct:email";
	public static final String ROUTE_NEW_ORDER = "direct:newOrderEmail";
	final ApplicationProperties props;

	public EmailRoute(ApplicationProperties props) {
		this.props = props;
	}

	@Override
	public void configure() throws Exception {
		from(ROUTE_BASIC).routeId(ROUTE_BASIC).to(props.getEmail()).id(ID_BASIC_EMAIL);
		
		//header order
		from(ROUTE_NEW_ORDER).routeId(ROUTE_NEW_ORDER)
			.setBody().simple("New order created: ${header.order}")
			.to(props.getEmail()).id(ID_BITCOIN_EMAIL);
		
//		from("timer://runOnce?repeatCount=1&delay=5000")
//		.setBody().simple("Hash power started")
//		.to(props.getEmail());
	}

}
