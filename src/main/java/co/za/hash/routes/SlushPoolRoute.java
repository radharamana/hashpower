package co.za.hash.routes;

import org.apache.camel.builder.RouteBuilder;

import co.za.hash.config.ApplicationProperties;
import co.za.hash.domain.DPool;
import co.za.hash.service.OrderService;
import co.za.hash.service.PoolService;

//@Component
public class SlushPoolRoute extends RouteBuilder{
	public static final String ID_REST = "slushRest";
	public static final String ROUTE_ID = "Slush";
	private final DPool slush;
	private final OrderService orderService;
	final PoolService poolService;
	private final ApplicationProperties props;
//	
//	
	public SlushPoolRoute(ApplicationProperties appProps, OrderService orderService, PoolService poolService) {
		super();
		this.props = appProps;
		this.slush = //appProps.getPool().getSlush();
				appProps.getDpools().get(0);
		this.orderService = orderService;
		this.poolService = poolService; 
	}

	//private SlushBlocks prevSlushBlocks; 
	
	@Override
	public void configure() throws Exception {
//		from("timer:slushpool?delay="+slush.getDelay()+"s&period="+slush.getPeriod()+"s")
//			.to("https://slushpool.com/stats/json/2067885-896f5e04453dafaa3f64fb8ef5dd6754").id(ID_REST)
//			.routeId(ROUTE_ID)
//			.unmarshal().json(JsonLibrary.Jackson, SlushBlocks.class)
//			.setHeader("pool").method(poolService, PoolService.M_DO_UPDATE_POOL+"('"+slush.getName()+"', ${body})")
//			.setBody().method(orderService, OrderService.M_GET_ORDER_FOR_POOL+"('"+slush.getName()+"')")
//			.choice()
//				.when().mvel("request.headers.pool.isBlockActive && request.body == null")
//					.to(NiceRoute.ROUTE_NEW_ORDER)//.id(NiceRoute.ROUTE_NEW_ORDER)
//				.when().mvel("request.headers.pool.isBlockActive && request.body != null && request.body.limitSpeed <= "+props.getNice().getAlgoMap().get("sha256").getMinSpeed())
//					.to(NiceRoute.ROUTE_SPEED_INC)
//					.to(NiceRoute.ROUTE_PRICE_INC)
//				.when().mvel("!request.headers.pool.isBlockActive && request.body != null")
//					.to(NiceRoute.ROUTE_SPEED_DEC)//.id(NiceRoute.ROUTE_SPEED)
//					.to(NiceRoute.ROUTE_PRICE_DEC)//.id(NiceRoute.ROUTE_DEC_PRICE)
//			.end();
	}
	
	
}
