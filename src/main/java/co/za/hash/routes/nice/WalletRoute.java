package co.za.hash.routes.nice;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import co.za.hash.camel.MyRouteBuilder;
import co.za.hash.config.ApplicationProperties;
import co.za.hash.service.WalletService;

@Component
public class WalletRoute extends MyRouteBuilder {
	public static final String ROUTE_ID = "Wallet";
	public static final String STR_BALANCE = "https://api.nicehash.com/api?method=balance&id=%s&key=%s";
	
	public static final String ID_REST = "balanceRest";
	

	final ApplicationProperties props;
	final WalletService walletService;
	
		
	public WalletRoute(ApplicationProperties props, WalletService walletService) {
		super(ROUTE_ID);
		this.props = props;
		this.walletService = walletService;
	}

	@Override
	public void configure() throws Exception {
		from(props.getRouteMap().get(ROUTE_ID).getTimer())
		.routeId(ROUTE_ID)
		.log("Getting wallet balance")
		.setHeader("rest").method(this,M_WALLET_REST) 
		.setHeader(Exchange.HTTP_METHOD).constant("GET").setBody().constant(null)
		.toD("${header.rest}").id(ID_REST)
		
		.transform().jsonpath("$.result.balance_confirmed")
		.bean(walletService, WalletService.M_SET_BTC)
		.log("Wallet updated to ${body}");
//		.choice()
//			.when().method(OrderService.class,"hasBalanceChanged")
//				.to("direct:refill")
//		.end();

	}
	
	private static final String M_WALLET_REST = "getWalletRest";
	public String getWalletRest() {
		return String.format(STR_BALANCE, props.getNice().getApiId(), props.getNice().getApiKey());
		
	}
}
