package co.za.hash.routes.nice;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

import co.za.hash.config.ApplicationProperties;
import co.za.hash.config.ApplicationProperties.MultiPool;
import co.za.hash.domain.APool;
import co.za.hash.domain.Market;
import co.za.hash.domain.nice.market.NiceHashMarket;
import co.za.hash.service.MarketService;
import co.za.hash.service.MultiPoolService;
import co.za.hash.service.OrderService;

@Component
public class MarketRoute extends RouteBuilder {
	
	private static final String SHA256 = "sha256";
	//public static final String ID_DEC3 = "dec3";
	//public static final String ID_DEC2 = "dec2";
	public static final String ID_DEC1 = "dec1";
	public static final String ID_REST = "rest";
	public static final String ID_INC = "inc";
	

	final OrderService orderService;
	final MarketService marketService;
	final MultiPoolService multiPoolService;
	final ApplicationProperties props;
	
	

	public MarketRoute(ApplicationProperties props, OrderService orderService, MarketService marketService, MultiPoolService multiPoolService) {
		super();
		this.orderService = orderService;
		this.props = props;
		this.marketService = marketService;
		this.multiPoolService = multiPoolService;
	}

	public static final String ROUTE_ID = "Market";
	
	@Override
	public void configure() throws Exception {
		from(props.getRouteMap().get(ROUTE_ID).getTimer())
		.routeId(ROUTE_ID)
		.loopDoWhile().method(orderService, OrderService.M_HAS_NEXT).setHeader("market").method(orderService,OrderService.M_NEXT)
			.setHeader(ID_REST).method(marketService,MarketService.M_GET_MARKET_REST)
			.setHeader(Exchange.HTTP_METHOD).constant("GET").setBody().constant(null)
			.log(LoggingLevel.DEBUG ,"Maintaining orders in market: ${header.market} ${header.rest}")
			.toD("${header.rest}").id(ID_REST)
			.unmarshal().json(JsonLibrary.Jackson, NiceHashMarket.class)
			.setHeader("minPrice").method(marketService,MarketService.M_DO_MIN_PRICE)
			.transform().method(orderService, OrderService.M_DO_FILTER_ORDERS )
			.split().simple("${body}")
			.choice()
				.when().simple("${body.market.algo.id} in 1,35")
					.setHeader("maxPrice").mvel("request.headers.market.getMaxPrice()").endChoice()
				.otherwise()
					.setHeader("pool").method(multiPoolService, MultiPoolService.M_DO_CONV_MPOOL)
					.setHeader("maxPrice").mvel("request.headers.market.getMaxPrice(request.headers.pool)")
			.end()
			.choice()
				.when().mvel("request.body.workers == 0 && request.headers.maxPrice > request.body.price && request.headers.market.minPrice <= request.headers.maxPrice")
					.log("btcAvailable:${body.btcAvailable}")
					.choice().when().groovy("request.body.btcAvailable < "+props.getNice().getOrder().getMin()).to(NiceRoute.ROUTE_REFILL).endChoice()
					.log("Increasing price maxPrice:${header.maxPrice}, order Price:${body.price}, min Price:${header.market.minPrice}")
					.to(NiceRoute.ROUTE_PRICE_INC).id(ID_INC)
				.when().mvel("request.body.workers != 0 && (request.body.price > request.headers.maxPrice || request.body.price > request.headers.minPrice * "+props.getPriceTol()+")")
					.to(NiceRoute.ROUTE_PRICE_DEC).id(ID_DEC1)
				.otherwise().log(LoggingLevel.DEBUG ,"No action taken for order: workers:${body.workers}, maxPrice:${header.maxPrice}, price:${body.price}, minPrice:${header.minPrice}").endChoice()
			.end()
		.end();
		
		
	}
	
	
	
	

}
