package co.za.hash.routes.nice;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import co.za.hash.camel.ArrayListAggregationStrategy;
import co.za.hash.camel.MyRouteBuilder;
import co.za.hash.config.ApplicationProperties;
import co.za.hash.domain.Market;
import co.za.hash.domain.nice.myorder.MyOrderAll;
import co.za.hash.service.MarketService;
import co.za.hash.service.OrderService;
import co.za.hash.service.PoolService;

@Component
public class MyOrdersRoute extends MyRouteBuilder{
	
	public static final String ID_REST_FIND_NEW = "restFindNewOrders";
	public static final String ID_REST_MAINTAIN = "restMaintainOrders";
	public static final String ROUTE_FIND_NEW = "FindNewOrders";
	public static final String ROUTE_MAINTAIN_ORDERS = "MaintainOrders";
	
	
	public static final String STR_REST = "https://api.nicehash.com/api?method=orders.get&my&id=%s&key=%s&location=%d&algo=%d";
	
	
	final ApplicationProperties props;
	final PoolService poolService;
	final OrderService orderService;
	final MarketService marketService;
	
		
	public MyOrdersRoute(ApplicationProperties props, PoolService poolService, OrderService orderService, MarketService marketService) {
		super(ROUTE_FIND_NEW);
		this.props = props;
		this.poolService = poolService;
		this.orderService = orderService;
		this.marketService = marketService;
	}

	@Override
	public void configure() throws Exception {
		from(props.getRouteMap().get(ROUTE_FIND_NEW).getTimer())
			.routeId(ROUTE_FIND_NEW)
			.loopDoWhile().method(marketService, MarketService.M_HAS_NEXT).setHeader("market").method(marketService, MarketService.M_NEXT)
				.log(LoggingLevel.DEBUG, "Looking for orders in market: ${header.market}")
				.setHeader("rest").method(this, M_GET_ORDER_REST)
				.setHeader(Exchange.HTTP_METHOD).constant("GET").setBody().constant(null)
				.toD("${header.rest}").id(ID_REST_FIND_NEW)
				.unmarshal().json(JsonLibrary.Jackson, MyOrderAll.class)
				.bean(orderService, OrderService.M_DO_UPDATE_MY_ORDERS_ALL)
			.end();

//		from(props.getRouteMap().get(ROUTE_MAINTAIN_ORDERS).getTimer())
//		.routeId(ROUTE_MAINTAIN_ORDERS)
//		.loopDoWhile().method(orderService,OrderService.M_HAS_NEXT).setHeader("market").method(orderService,OrderService.M_NEXT )
//			.setHeader("rest").method(this, M_GET_ORDER_REST)
//			.setHeader(Exchange.HTTP_METHOD).constant("GET").setBody().constant(null)
//			.toD("${header.rest}").id(ID_REST_MAINTAIN)
//			.unmarshal().json(JsonLibrary.Jackson, MyOrderAll.class)
//			.bean(orderService, OrderService.M_DO_UPDATE_MY_ORDERS_ALL)
//			.split().simple("${body}")
//			.choice().when().mvel("request.body.btcAvailable < "+props.getNice().getOrder().getMin()+" && request.body.pool.isBlockActive")
//				.to(NiceRoute.ROUTE_REFILL)
//			.end()
//		.end().to(NiceRoute.ROUTE_RECREATE_EXPIRED);
	}
	
	private static final String M_GET_ORDER_REST = "getOrderRest(${header.market})";
	public String getOrderRest(Market market) {
		return String.format(STR_REST, props.getNice().getApiId(), props.getNice().getApiKey(), market.getLoc().ordinal(), market.getAlgo().getId());
	}

	@Bean
    private AggregationStrategy batchAggregationStrategy() {
            return new ArrayListAggregationStrategy();
    }

}
