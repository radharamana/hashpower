package co.za.hash.routes.nice;

import java.math.BigDecimal;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.Handler;
import org.apache.camel.Headers;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import co.za.hash.camel.MyRouteBuilder;
import co.za.hash.config.ApplicationProperties;
import co.za.hash.config.ApplicationProperties.NiceHash;
import co.za.hash.domain.Market;
import co.za.hash.domain.Order;
import co.za.hash.domain.nice.NiceAuth;
import co.za.hash.domain.nice.market.NiceHashMarket;
import co.za.hash.routes.EmailRoute;
import co.za.hash.service.MarketService;
import co.za.hash.service.OrderService;
import co.za.hash.service.PoolService;
import co.za.hash.service.WalletService;

@Component
public class NiceRoute extends MyRouteBuilder {
	
	public static final String ID_ORDER_FOR_POOL_ALREADY = "orderForPoolAlready";
	public static final String ID_DEC_PRICE_REST = "decPriceRest";
	public static final String ID_INC_PRICE_REST = "incPriceRest";
	public static final String ID_SPEED_REST = "speedRest";
	public static final String ID_NEW_ORDER_REST = "newOrderRest";
	public static final String ID_REFILL_REST = "refillRest";
	
	
	public static final String ROUTE_PRICE_INC = "direct:increasePrice";
	public static final String ROUTE_PRICE_DEC = "direct:decreasePrice";
	public static final String ROUTE_SPEED = "direct:speed";
	public static final String ROUTE_SPEED_INC = "direct:speedInc";
	public static final String ROUTE_SPEED_DEC = "direct:speedDec";
	public static final String ROUTE_RECREATE_EXPIRED = "direct:recreateExpired";
	
	public static final String ROUTE_REFILL = "direct:refill";
	public static final String ROUTE_NEW_ORDER = "direct:newOrder";
	
	public static final String STR_DEC_PRICE_REST = "https://api.nicehash.com/api?method=orders.set.price.decrease&id=%s&key=%s&location=%d&algo=%d&order=%d";
	public static final String STR_SPEED_REST = "https://api.nicehash.com/api?method=orders.set.limit&id=%s&key=%s&location=%d&algo=%d&order=%d&limit=%.2f";
	public static final String STR_NEW_ORDER_REST = "https://api.nicehash.com/api?method=orders.create&id=%s&key=%s&location=%d&algo=%d&amount=%f&price=%.4f&limit=%.2f&pool_host=%s&pool_port=%s&pool_user=%s&pool_pass=%s&code=%s";
	public static final String STR_INC_PRICE_REST = "https://api.nicehash.com/api?method=orders.set.price&id=%s&key=%s&location=%d&algo=%d&order=%d&price=%.4f";
	public static final String STR_REFILL_REST = "https://api.nicehash.com/api?method=orders.refill&id=%s&key=%s&location=%d&algo=%d&order=%d&amount=%f";
	
	
	
	final NiceHash nice;
	
	private NiceHashMarket niceHashMarket;
	private final WalletService walletService;
	final PoolService poolService;
	final NiceAuth niceAuth;
	final OrderService orderService;

	public NiceRoute(ApplicationProperties appProps, WalletService wallet, OrderService orderService, NiceAuth niceAuth, PoolService poolService) {
		super("nice");
		nice = appProps.getNice();
		walletService = wallet;
		this.orderService = orderService;
		this.niceAuth = niceAuth;
		this.poolService = poolService;
		
	}

	@Override
	public void configure() throws Exception {
		from(ROUTE_SPEED_INC)
			.routeId(ROUTE_SPEED_INC)
			.setHeader("speed").simple("${body.pool.limitSpeed}")
			.to(ROUTE_SPEED);
		
		from(ROUTE_SPEED_DEC)
			.routeId(ROUTE_SPEED_DEC)
			.setHeader("speed").method(poolService, PoolService.M_GET_ALGO_MIN_SPEED)
			.to(ROUTE_SPEED);
		
		from(ROUTE_SPEED)
			.routeId(ROUTE_SPEED)
			.setHeader("order").simple("${body}")
			.setHeader("rest").method(this, M_SPEED_REST) //.mvel("String.format('"+STR_SPEED_REST+"','"+nice.getApiId()+"','"+nice.getApiKey()+"', request.body.pool.loc,request.body.pool.algo,request.body.id,request.headers.speed)")
			.setHeader(Exchange.HTTP_METHOD).constant("GET").setBody().constant(null)
			.toD("${header.rest}").id(ID_SPEED_REST)
			.convertBodyTo(String.class)
			.choice()
				.when().jsonpath("$.result.success", true)
					.log("${body}")
					.transform().mvel("request.headers.order.limitSpeed = request.headers.speed")
					.endChoice()
				.otherwise().log(LoggingLevel.ERROR, "Error: ${body}")
			.end().setBody().simple("${header.order}");
		
		//.setBody().jsonpathWriteAsString("$.result")
		
		from(ROUTE_PRICE_DEC)
			.routeId(ROUTE_PRICE_DEC)
			.setHeader("order").simple("${body}")
			.setHeader("rest").method(this,M_DEC_PRICE_REST) 
			.setHeader(Exchange.HTTP_METHOD).constant("GET").setBody().constant(null)
			.log("Attempting to decrease price for order: ${body} ${header.rest}")
			.toD("${header.rest}").id(ID_DEC_PRICE_REST)
			.convertBodyTo(String.class)
			.choice()
				.when().jsonpath("$.result.success", true)
					.log("${body}")
					.transform().mvel("request.headers.order.price = request.headers.order.price - "+nice.getDecAmount())
					.endChoice()
				.when().jsonpath("$.result.error")
					.log(LoggingLevel.ERROR, "Error: ${body}")
					.choice().when().mvel("request.headers.order.price > request.headers.maxPrice*1.1 && request.headers.order.limitSpeed > request.headers.order.market.algo.minSpeed")
						.log("Decrease price failed decreasing speed price:${header.order.price}, maxPrice:${header.maxPrice},limitSpeed:${header.order.limitSpeed}, minSpeed:${header.order.market.algo.minSpeed}")
						.setBody().simple("${header.order}")
						.to(ROUTE_SPEED_DEC)
					.end()
				
			.end().setBody().simple("${header.order}");
		
		//depends on header.pool
		from(ROUTE_NEW_ORDER)
			.routeId(ROUTE_NEW_ORDER)
			.choice()
					.when().method(walletService,WalletService.M_IS_WALLET_EMPTY).log("Order not created: Wallet empty")
					.when().method(orderService,OrderService.M_IS_ORDER_FOR_POOL).log(LoggingLevel.ERROR, "Order already exists for market(${header.market})").id(ID_ORDER_FOR_POOL_ALREADY)
						//.bean(orderService,OrderService.M_DEL_ORDER)
			.otherwise()
				//.setHeader("key").method(niceAuth,NiceAuth.M_GET_KEY)
				.setHeader("order").method(orderService, OrderService.M_DO_CREATE_ORDER)
				//.setHeader("expired").method(this, M_GET_EXPIRED_ID) //.mvel("request.body.size && request.body[0].expired?body[0].id:0")
				.setHeader("rest").method(this, M_GET_NEW_ORDER_REST)
				.setHeader(Exchange.HTTP_METHOD).constant("GET").setBody().constant(null)
				.toD("${header.rest}").id(ID_NEW_ORDER_REST)
				.log("${header.rest}")
				.convertBodyTo(String.class)
				.log("${body}")
				.choice()
					.when().jsonpath("$.result.success", true)
						//.transform().jsonpath("$.result.success", true)
						.setHeader("id").method(this,NiceRoute.M_GET_ID_REG)
						.bean(orderService,OrderService.M_ADD_ORDER).log(LoggingLevel.INFO,"${body}")
						.choice().when().simple("${header.expired} != null").bean(orderService, OrderService.M_DEL_ORDER).otherwise().endChoice()
						.to(EmailRoute.ROUTE_NEW_ORDER)
					.when().jsonpath("$.result.error").log(LoggingLevel.ERROR, "Error: ${body}")
				.end()
			.end();
		
		from(ROUTE_PRICE_INC)
			.routeId(ROUTE_PRICE_INC)
			.setHeader("order").simple("${body}")
			.choice().when().groovy("request.headers.order.limitSpeed < request.headers.order.pool.limitSpeed")
				.to(ROUTE_SPEED_INC)
			.end()
			.setHeader("rest").method(this,M_GET_INC_PRICE_REST)
			.setHeader(Exchange.HTTP_METHOD).constant("GET").setBody().constant(null)
			.log("Attempting to increase price for order:${body} ${header.rest}")
			.toD("${header.rest}").id(ID_INC_PRICE_REST)
			.convertBodyTo(String.class)
			.log("${body}")
			.choice()
				.when().jsonpath("$.result.success", true)
					.log("${body}")
					.transform().mvel("request.headers.order.price = request.headers.order.market.minPrice")
					.endChoice()
				.when().jsonpath("$.result.error").log(LoggingLevel.ERROR, "${body} ")
			.end().setBody().simple("${header.order}");
		
		
		//body = order
		from(ROUTE_REFILL).setHeader(Exchange.HTTP_METHOD, constant("GET"))
			.routeId(ROUTE_REFILL)
			.choice().when().method(walletService,WalletService.M_IS_WALLET_EMPTY).endChoice()
			.otherwise()
				.setHeader("order").simple("${body}")
				.setHeader("btc").method(walletService, WalletService.M_GET_BTC_REFILL)
				.setHeader("rest").method(this, M_REFILL_REST)
				.toD("${header.rest}").id(ID_REFILL_REST)
				.convertBodyTo(String.class)
				.choice()
					.when().jsonpath("$.result.success", true)
						.transform().mvel("request.headers.order.btcAvailable = request.headers.order.btcAvailable + request.headers.btc")
						.log(LoggingLevel.INFO,"${body}")
					.when().jsonpath("$.result.error").log(LoggingLevel.ERROR, "Error: ${body}")
				.end()
				.setBody().simple("${header.order}")
			.end();
		
		from(ROUTE_RECREATE_EXPIRED)
			.bean(orderService, OrderService.M_GET_EXPIRED)
			//.log()
			.choice().when().simple("${body.size} == 0")
			.otherwise()//.mvel(/*"request.body.size > 0"*/"1 == 0")
				.split().simple("${body}")
					.setHeader("pool").simple("${body.pool}")
					.setHeader("expired").simple("${body.id}")
					.to(NiceRoute.ROUTE_NEW_ORDER);
				
			//.otherwise().log("there");
	}

	
	public boolean getF(){
		return false;
	}
	
	
	public NiceHashMarket getNiceHashMarket() {
		return niceHashMarket;
	}


	@Handler
	public void setNiceHashMarket(@Body NiceHashMarket niceHashMarket) {
		this.niceHashMarket = niceHashMarket;
	}
	
	private static final String M_GET_ID_REG = "getIdReg";
	Pattern idReg = Pattern.compile("\\d+");
	public Integer getIdReg(String msg){
		if("".equals(msg) || msg==null)return null;
		Matcher m = idReg.matcher(msg);
		m.find();
		try{
			int id = Integer.parseInt(m.group(0));
			return id;
		}catch(IllegalStateException e){
			log.error("No match found for msg:{}",msg);
			return null;
		}
	}
	
	private static final String M_SPEED_REST = "getSpeedRest";
	public String getSpeedRest(@Body Order order, @Headers Map<String, Object> headers){
		return String.format(STR_SPEED_REST, nice.getApiId(), nice.getApiKey(), order.getMarket().getLoc().ordinal(), order.getMarket().getAlgo().getId(), order.getId(), headers.get("speed"));
	}
	
	private static final String M_DEC_PRICE_REST = "getDecPriceRest";
	public String getDecPriceRest(@Body Order order) {
		return String.format(STR_DEC_PRICE_REST, nice.getApiId(), nice.getApiKey(), order.getMarket().getLoc().ordinal(), order.getMarket().getAlgo().getId(), order.getId());
		
	}
	
	private static final String M_GET_NEW_ORDER_REST = "getNewOrderRest(${header.order}, ${header.market})";
	public String getNewOrderRest(Order order, Market market) {
		return String.format(STR_NEW_ORDER_REST, nice.getApiId(), nice.getApiKey(), market==null?order.getPool().getLoc():market.getLoc().ordinal(), order.getMarket().getAlgo().getId(), order.getBtcAvailable(), order.getPrice(), order.getLimitSpeed(), order.getPool().getHost(), order.getPool().getPort(), order.getPool().getUser()+market.getId(), order.getPool().getPass(), niceAuth.getKey());
		//"https://api.nicehash.com/api?method=orders.create&id=%s&key=%s&location=%d&algo=%d&amount=%f&price=%.4f&limit=%.2f&pool_host=%s&pool_port=%s&pool_user=%s&pool_pass=%s&code=%s";
	}

	private static final String M_GET_INC_PRICE_REST = "getIncPriceRest(${body})";
	public String getIncPriceRest(Order order) {
		return String.format(STR_INC_PRICE_REST, nice.getApiId(), nice.getApiKey(), order.getMarket().getLoc().ordinal(), order.getMarket().getAlgo().getId(), order.getId(), order.getMarket().getMinPrice());

	}
	
	private static final String M_REFILL_REST = "getRefillRest(${body},${header.btc})";
	public String getRefillRest(Order order, BigDecimal btc) {
		return String.format(STR_REFILL_REST, nice.getApiId(), nice.getApiKey(), order.getMarket().getLoc().ordinal(), order.getMarket().getAlgo().getId(), order.getId(), btc);

	}
	
	
}
