package co.za.hash.routes;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

import co.za.hash.config.ApplicationProperties;
import co.za.hash.domain.APool;
import co.za.hash.domain.Wtm;
import co.za.hash.domain.nice.market.NiceHashMarket;
import co.za.hash.routes.nice.MarketRoute;
import co.za.hash.routes.nice.NiceRoute;
import co.za.hash.service.MarketService;
import co.za.hash.service.PoolService;

@Component
public class BitcoinRoute extends RouteBuilder {
	public static final String ID_NICE_BTC_MARKET = "NiceBtcMarket";
	public static final String ID_WTM_REST = "WhatToMineRest";
	//public static final String URL_WTM = "http4://whattomine.com/coins/1.json?p=0&fee=5.9&cost=0&hcost=0.0&hr=1000000";
	public static final String ROUTE_ID = "Bitcoin";
	final ApplicationProperties props;
	final MarketService marketService;
	final APool bitPool;
	
	public BitcoinRoute(ApplicationProperties props, MarketService marketService, PoolService poolService){
		this.props = props;
		this.marketService  = marketService;
		this.bitPool = poolService.getPool("bitcoin").get();
	}

	@Override
	public void configure() throws Exception {
		from(props.getRouteMap().get(ROUTE_ID).getTimer())
		.routeId(ROUTE_ID)
		.log("Getting bitcoin price from what to mine")
		.to(props.getRouteMap().get(ROUTE_ID).getUrl()).id(ID_WTM_REST)
		.unmarshal().json(JsonLibrary.Jackson, Wtm.class)
		.setHeader("maxPrice").simple("${body.btcRevenue}")
		.bean(marketService,MarketService.SET_BTC_MAX_PRICE)
		.log("Getting bitcoin price from nicehash")
		
		.loopDoWhile().method(marketService, MarketService.M_HAS_NEXT_BTC)
			.setHeader("market").method(marketService, MarketService.M_NEXT_BTC)
			.setHeader("rest").method(marketService,MarketService.M_GET_MARKET_REST)
			.setHeader(Exchange.HTTP_METHOD).constant("GET").setBody().constant(null)
			.toD("${header.rest}").id(ID_NICE_BTC_MARKET)
			.unmarshal().json(JsonLibrary.Jackson, NiceHashMarket.class)
			.setHeader("minPriceUs").method(marketService,MarketService.M_DO_MIN_PRICE)
		.end()
		
		.setHeader("market").method(marketService,MarketService.M_GET_BTC_CHEAPEST)
		.setHeader("pool").constant(bitPool)
		//.choice().when().groovy("request.headers.startPrice >= request.headers.minPriceUs || < request.headers.minPriceEu")
		.choice().when().groovy("request.headers.market.minPrice <= request.headers.maxPrice")
			.to(NiceRoute.ROUTE_NEW_ORDER)
			
		.otherwise()
			.log("Nice Hash cheapest market:${header.market}, BTC price:${header.maxPrice}. Nice prices are too high.")
		.endChoice()
		;
		
		
	}

}
