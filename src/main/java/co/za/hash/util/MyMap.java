package co.za.hash.util;

public class MyMap<P,V> {
	final P p;
	final V v;
	public MyMap(P p, V v){
		this.p = p;
		this.v = v;
	}
	public P getParent() {
		return p;
	}
	public V getValue() {
		return v;
	}
	
	
}
