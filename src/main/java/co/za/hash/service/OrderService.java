package co.za.hash.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import co.za.hash.config.ApplicationProperties;
import co.za.hash.config.ApplicationProperties.Algo;
import co.za.hash.domain.APool;
import co.za.hash.domain.Market;
import co.za.hash.domain.Order;
import co.za.hash.domain.nice.market.NiceHashMarket;
import co.za.hash.domain.nice.myorder.MyOrder;
import co.za.hash.domain.nice.myorder.MyOrderAll;

@Service
public class OrderService implements Iterator<Market>{
	
	private static final Logger log = LoggerFactory.getLogger(OrderService.class);

	//final CamelContext context;
	//final ProducerTemplate template;
	final PoolService poolService;
	final ApplicationProperties props;
	final WalletService walletService;
	
	final static float MIN_SPEED = 0.05f;
	
	public OrderService(ApplicationProperties props, PoolService poolService, WalletService wallet) {
		super();
		//this.template = context.createProducerTemplate();
		this.poolService = poolService;
		this.props = props;
		this.walletService = wallet;
	}

	Map<Integer, Order> orders = new HashMap<Integer, Order>();
	Map<Algo, Market> prices;
	//BigDecimal minPrice;
	
	
	public static final String M_DO_FILTER_ORDERS = "doFilterMyOrders(${body},${header.market})";
	public List<Order> doFilterMyOrders(NiceHashMarket niceHashMarket, Market market){
		
		List<Order> ordersInMarket = niceHashMarket.getResult().getOrders().stream()
			.filter(o->orders.containsKey(o.getId()))
			.map(o-> update(orders.get(o.getId()),o)).collect(Collectors.toList());
		
		List<Order> orderNotInMarket = orders.values().stream().filter(o->!ordersInMarket.contains(o) && o.getMarket().equals(market)).collect(Collectors.toList());
		doHandleExpired(orderNotInMarket);
		return ordersInMarket;
	}
	
	private Order update(Order oldO, Order newO){
		return oldO.setAlive(newO.getAlive())
		.setLimitSpeed(newO.getLimitSpeed())
		.setPrice(newO.getPrice())
		.setAcceptedSpeed(newO.getAcceptedSpeed())
		.setWorkers(newO.getWorkers());
	}
	
	private Order doTransMyOrder(MyOrder myOrder, Market market){
		Order order = new Order();
		
		order.setLimitSpeed(myOrder.getLimitSpeed());
		order.setAlive(myOrder.getAlive());
		order.setPrice(myOrder.getPrice());
		order.setId(myOrder.getId());
		order.setType(myOrder.getType());
		order.setWorkers(myOrder.getWorkers());
		order.setAcceptedSpeed(myOrder.getAcceptedSpeed());
		order.setPool(poolService.getPool(myOrder));
		order.setBtcAvailable(myOrder.getBtcAvail());
		order.setMarket(market);
		
		return order;
	}
	

	
	
	//public static final String M_GET_MIN_PRICE = "getMinPrice()";
	

	public Map<Integer, Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders.stream().map(o->o.setPool(poolService.getPool(o.getPool().getName()).get()))
				.collect(Collectors.toMap(Order::getId, Function.identity()));
	}
	
	
	public static final String M_ADD_ORDER = "addOrder(${header.id},${header.order})";	
	public void addOrder(Integer id, Order order){
		if(order == null)return;
		if(id == null)id = - new Long((new Date()).getTime()).intValue();
		order.setId(id);
		orders.put(order.getId(), order);
		
		walletService.doDecBtc(order);
	}
	
		
	public static final String M_DO_UPDATE_MY_ORDERS_ALL = "doUpdateMyOrderAll(${body}, ${header.market})";
	public List<Order> doUpdateMyOrderAll(MyOrderAll myOrderAll, Market market){
		List<Order> newOrders = myOrderAll.getResult().getOrders().stream()
			.map(o->doTransMyOrder(o, market)).collect(Collectors.toList());
		
		//finding orders with no id
		List<Order> noIdOrders = this.orders.values().stream().filter(o -> o.getId() < 0 && o.getMarket().equals(market)).collect(Collectors.toList());
		
	
		newOrders.stream().forEach(o->{
			//Optional<Order> nido = 
			noIdOrders.stream().filter(nid->nid.getPool().equals(o.getPool())).findFirst().ifPresent(nid->nid.setId(o.getId()));
			if(this.orders.put(o.getId(), o)==null){
				log.info("New Order added: {}", o);
			}
		});

		
		List<Order> expiredOrders = this.orders.values().stream()
				.filter(o->!newOrders.contains(o) && o.getMarket().equals(market))
				.collect(Collectors.toList());
		
		doHandleExpired(expiredOrders);
		
		return newOrders;
	}

	private void doHandleExpired(List<Order> expiredOrders) {
		for(Order order:expiredOrders){
			//if(order.getPool().isBlockActive())order.setExpired(true);
			log.info("Deleting expired order: {}", order);
			this.orders.remove(order.getId());
		}
	}
	
	public Set<Market> getMarketsUsed(){
		return orders.values().stream().map(o->o.getMarket()).collect(Collectors.toSet());
	}
	
	public static final String M_DO_CREATE_ORDER = "doCreateOrder(${header.pool.name}, ${header.market})";
	public Order doCreateOrder(String poolName, Market market){
		APool pool = poolService.getPool(poolName).get();
		return new Order()
				.setPrice(market==null?pool.getPrefMarket().getMinPrice():market.getMinPrice())
				.setBtcAvailable(walletService.getBtcForOrder(market))
				.setLimitSpeed(pool.getLimitSpeed())
				.setMarket(market)
				.setPool(pool);
	}
	
	Set<Market> marketsUsed;
	Iterator<Market> iter;
	
	public static final String M_HAS_NEXT = "hasNext";
	public boolean hasNext(){
		if(marketsUsed == null){
			marketsUsed = getMarketsUsed();
			iter = marketsUsed.iterator();
		}
		boolean result = iter.hasNext();
		if(!result){
			marketsUsed=null; iter=null;
		}
		return result;
	}
	
	
	public static final String M_NEXT = "next";
	public Market next(){
		return iter.next();
	}

	public static final String M_GET_ORDER_FOR_POOL = "getOrderForPool";
	public Order getOrderForPool(String poolName){
		APool pool = poolService.getPool(poolName).get();
		return orders.values().stream().filter(o->o.getPool().equals(pool) && !o.isExpired()).findFirst().orElse(null);
	}
	
//	public static final String M_DO_UPDATE_PRICE_INC = "doUpdatePriceInc";
//	public Order doUpdatePriceInc(@Body Order order){
//		return order.setPrice(minPrice);
//	}
	
	public static final String M_GET_EXPIRED = "getExpired";
	
	public List<Order> getExpired(){
		List<Order> exp = this.orders.values().stream().filter(Order::isExpired).collect(Collectors.toList());
		return exp;
	}
	
	public static final String M_DEL_ORDER = "doDelOrder(${header.expired})";
	public void doDelOrder(Integer id){
		this.orders.remove(id);
	}
	
	public static final String M_IS_ORDER_FOR_POOL = "isOrderForPool(${header.pool}, ${header.market})";
	public boolean isOrderForPool(APool pool, Market market){
		boolean present = this.orders.values().stream()
						.filter(o->o.getPool().equals(pool) && !o.isExpired() && o.getMarket().equals(market))
						.findFirst().isPresent();
		return present;
	}
	
}
