package co.za.hash.service;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.google.common.collect.Ordering;

import co.za.hash.config.ApplicationProperties;
import co.za.hash.config.ApplicationProperties.Algo;
import co.za.hash.config.ApplicationProperties.MultiPool;
import co.za.hash.domain.AlgoPrice;
import co.za.hash.domain.APool;
import co.za.hash.domain.mpool.YimpStatus;
import co.za.hash.domain.mpool.YimpStatusB;
import co.za.hash.domain.mpool.YimpStatusC;
import co.za.hash.domain.mpool.hub.HubStatus;
import co.za.hash.domain.nice.global.Stat;
import co.za.hash.util.ValueComparableMap;

@Component
public class MultiPoolService implements Iterator<MultiPool>{
	
	private static final Logger log = LoggerFactory.getLogger(MultiPoolService.class);

	List<MultiPool> pools;
	Iterator<MultiPool> iter;
	Map<Algo, AlgoPrice> algoPrices = new HashMap<Algo, AlgoPrice>();
	//TreeMap<Algo, BigDecimal> algoPrices = new ValueComparableMap<Algo, BigDecimal>(Ordering.natural());
	
	final ApplicationProperties props;
	public MultiPoolService(ApplicationProperties props){
		this.props = props;
		this.pools = props.getMultiPools();
		props.getNice().getAlgos().stream().forEach(a->algoPrices.put(a, new AlgoPrice(props, a)));
	}

	public static final String M_HAS_NEXT = "hasNext";
	@Override
	public boolean hasNext() {
		if(iter == null){	
			iter = pools.iterator();
			return iter.hasNext();
		}else if(!iter.hasNext()){
			iter = null;
			return false;
		}
		return true;
	}

	public static final String M_NEXT = "next";
	@Override
	public MultiPool next() {
		return iter.next();
	}
	
	public static final String M_DO_PRICE_UPDATE_pool_yimp = "doPriceUpdate(${header.pool}, ${body})";
	public void doPriceUpdate(MultiPool pool, YimpStatus status){
		status.getAlgos().values().stream().forEach(a->{
			Optional<Algo> algo = getAlgoForName(a.getName());
			if(algo.isPresent()){
				Algo al = algo.get();
				AlgoPrice alPrice = algoPrices.get(al);
				BigDecimal minPrice = a.getEstimateCurrent().min(a.getEstimateLast24h()).min(a.getActualLast24h().divide(BigDecimal.valueOf(1000l)));
				if(alPrice == null){
					algoPrices.get(al);
					log.error("No AlgoPrice for algo: {}, algoPrices: {}", al, algoPrices.keySet().iterator().next().equals(al));
				}else alPrice.addMultiPrice(pool, minPrice,a.getFees(), al);
			}else{
				log.debug("Algo:{} not supported.",a.getName());
			}
		});
	}
	
	
	public static final String M_DO_PRICE_UPDATE_pool_hub = "doPriceUpdate(${header.pool}, ${body})";
	public void doPriceUpdate(MultiPool pool, HubStatus status){
		
		status.getReturn().stream().forEach(a->{
			Optional<Algo> algo = getAlgoForName(a.getAlgo());
			if(algo.isPresent()){
				Algo al = algo.get();
				AlgoPrice alPrice = algoPrices.get(al);
				BigDecimal minPrice = a.getProfit();
				if(alPrice == null){
					algoPrices.get(al);
					log.error("No AlgoPrice for algo: {}, algoPrices: {}", al, algoPrices.keySet().iterator().next().equals(al));
				}else alPrice.addMultiPrice(pool, minPrice,BigDecimal.ZERO, al);
			}else{
				log.debug("Algo:{} not supported.",a.getAlgo());
			}
		});
	}
	
	
	public static final String M_DO_NICE_PRICE_UPDATE_nice = "doNicePriceUpdate(${body})";
	public Map<Algo, AlgoPrice> doNicePriceUpdate(Stat stats){
		stats.getResult().getStats().stream().forEach(s->{
			Optional<Algo> algo = getAlgoForIndex(s.getAlgo());
			if(algo.isPresent()){
				AlgoPrice price = algoPrices.get(algo.get());
				if(price == null)log.error("No algo price for algo({})",algo.get());
				else price.setNiceHashPrice(s.getPrice());
			}
		});
		return algoPrices;
	}

	public Optional<Algo> getAlgoForName(String name){
		return props.getNice().getAlgos().stream().filter(a->a.getNames().contains(name.toLowerCase())).findFirst();
	}
	
	public Optional<Algo> getAlgoForIndex(Integer index){
		return props.getNice().getAlgos().stream().filter(a->a.getId()==index).findFirst();
	}
	
	public static final String M_TO_STRING = "toString";
	@Override
	public String toString(){
		return algoPrices.values().stream()
				.filter(a->a.getBestPool() != null && a.getProfitPercent().compareTo(BigDecimal.ZERO) > 0)
				.sorted(new Comparator<AlgoPrice>() {@Override public int compare(AlgoPrice ap1, AlgoPrice ap2) { return ap1.compareTo(ap2);}})
				.map(a->a.toSummaryString())
				.collect(Collectors.joining("\n", "\n", "\n"));
	}
	
//	public BigDecimal getBestPriceForAlgo(Algo algo, BigDecimal profitPercent){
//		return algoPrices.get(algo).getProfitPrice(profitPercent);
//	}
	
	public BigDecimal getPriceForAlgoForPool(Algo algo, MultiPool pool, BigDecimal profitPercent){
		return algoPrices.get(algo).getProfitPrice(pool, profitPercent);
	}

	public Set<MultiPool> getPoolsForAlgo(Algo algo) {
		return algoPrices.get(algo).getPools();
	}
	
	public static final String M_DO_CONV_MPOOL = "doConvMultiPool(${body.pool})";
	public MultiPool doConvMultiPool(APool pool){
		return pools.stream().filter(p->p.getName().toLowerCase().equals(pool.getName())).findFirst().get();
	}
	
}
