package co.za.hash.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import co.za.hash.config.ApplicationProperties;
import co.za.hash.config.ApplicationProperties.Algo;
import co.za.hash.config.ApplicationProperties.MultiPool;
import co.za.hash.domain.Location;
import co.za.hash.domain.APool;
import co.za.hash.domain.Market;
import co.za.hash.domain.nice.global.Stat;
import co.za.hash.domain.nice.market.NiceHashMarket;

@Component
public class MarketService implements Iterator<Market>{
	
	private static final Logger log = LoggerFactory.getLogger(MarketService.class);

	final ApplicationProperties props;
	final Map<Integer, Market> markets;
	final List<Market> btcMarkets;
	final MultiPoolService multiPoolService;
	Iterator<Market> iter;
	Iterator<Market> btcIter;
	
	
	public MarketService(ApplicationProperties props, MultiPoolService multiPoolService) {
		super();
		this.props = props;
		this.multiPoolService = multiPoolService;
		
		markets = props.getNice().getAlgos().stream()
			.flatMap(a->Arrays.asList(new Market(Location.EU, a), new Market(Location.US, a)).stream())
			.collect(Collectors.toMap(Market::getId, Function.identity()));
		Algo btc = props.getNice().getAlgoMap().get("sha256");
		Algo btca = props.getNice().getAlgoMap().get("sha256a");
		btcMarkets = Arrays.asList(get(Location.US.ordinal(), btc.getId())
				,get(Location.EU.ordinal(), btc.getId())
				,get(Location.US.ordinal(), btca.getId())
				,get(Location.EU.ordinal(), btca.getId())
			);
		
	}

	
	public static final String M_HAS_NEXT = "hasNext";
	@Override
	public boolean hasNext(){
		if(iter == null){
			iter = markets.values().iterator();
		}
		boolean result = iter.hasNext();
		if(!result)iter = null;
		return result;
	}
	
	public static final String M_NEXT = "next";
	@Override
	public Market next(){
		return iter.next();
	}
	
	public static final String M_HAS_NEXT_BTC = "hasNextBtc";
	public boolean hasNextBtc(){
		if(btcIter == null){
			btcIter = btcMarkets.iterator();
		}
		boolean result = btcIter.hasNext();
		if(!result)btcIter = null;
		return result;
	}
	
	public static final String M_NEXT_BTC = "nextBtc";
	public Market nextBtc(){
		return btcIter.next();
	}
	
	public static final String M_DO_MIN_PRICE = "doMinPrice(${body},${header.market})";
	public BigDecimal doMinPrice(NiceHashMarket market, Market myMarket) {
//		BigDecimal minPrice = market.getResult().getOrders().stream()
//			.filter(o->o.getType()==0)
//			.filter(o->o.getLimitSpeed().floatValue()*1000000*props.getPriceTol().floatValue()<o.getAcceptedSpeed())
//			.reduce((first, second) -> second).get().getPrice();
		
		BigDecimal minPrice = market.getResult().getOrders().stream()
			.filter(o->o.getType()==0 && o.getWorkers()!=0)
			.reduce((first, second) -> second==null?first:second ).get().getPrice();
		
		BigDecimal minAdd = BigDecimal.ONE.divide(BigDecimal.TEN.pow(minPrice.scale()),minPrice.scale() , RoundingMode.UNNECESSARY);
		myMarket.setMinPrice(minPrice.add(minAdd));
		myMarket.setMinAdd(minAdd);
		//prices.put(pool.get, value)
		log.info("Setting minPrice to {} for market {}",myMarket.getMinPrice(), myMarket);
		return myMarket.getMinPrice();
	}
	public Market get(int location, int algo) {
		return this.markets.get(Market.getId(location, algo));
	}
	
	public static final String M_UPDATE_MAX_PRICE = "doUpdateMaxPrices";

	
	public void doUpdateMaxPrices(){
		for(Market market: markets.values()){
			for(MultiPool pool:multiPoolService.getPoolsForAlgo(market.getAlgo())){
				market.setMaxPrice(pool, multiPoolService.getPriceForAlgoForPool(market.getAlgo(), pool, props.getProfitPercent()));
			}
		}
		//markets.values().forEach(m->m.setMaxPrice(multiPoolService.getBestPriceForAlgo(m.getAlgo(), props.getProfitPercent())));
	}
	
	public List<Market> getMarkets(Algo algo){
		return markets.values().stream().filter(m->m.getAlgo().equals(algo)).collect(Collectors.toList());
	}
	
	public static String SET_BTC_MAX_PRICE = "setBtcMaxPrice(${header.maxPrice})";
	public void setBtcMaxPrice(BigDecimal poolMaxPrice) {
		if(poolMaxPrice.compareTo(props.getNice().getAlgoMap().get("sha256").getMaxPrice())==-1){
			this.btcMarkets.forEach(b->b.setMaxPrice(poolMaxPrice));
		}else{
			log.error("Max Price:{} is above limit:{}",poolMaxPrice,props.getNice().getAlgoMap().get("sha256").getMaxPrice());
		}
		
	}
	
	public static final String M_GET_BTC_CHEAPEST = "getBtcCheapestMarket";
	public Market getBtcCheapestMarket(){
		Collections.sort(btcMarkets, new Comparator<Market>() {
			@Override
			public int compare(Market m1, Market m2) {
				int minPriceComp = m1.getMinPrice().compareTo(m2.getMinPrice());
				if(minPriceComp == 0){
					if(m1.getLoc() == Location.US && m2.getLoc() == Location.US){
						return 0;
					}else if(m1.getLoc() == Location.US){
						return -1;
					}else{
						return 1;
					}
				}else{
					return minPriceComp;
				}
			}
		});
		return btcMarkets.get(0);
		//return cheapestMarket.getMinPrice().compareTo(cheapestMarket.getMaxPrice())==-1?cheapestMarket:null;
	}
	
	private static final String STR_MARKET_REST = "https://api.nicehash.com/api?method=orders.get&location=%d&algo=%d";
	public static final String M_GET_MARKET_REST = "getMarketRest(${header.market})";
	public String getMarketRest(Market market) {
		return String.format(STR_MARKET_REST, market.getLoc().ordinal(), market.getAlgo().getId());
	}
	
}
