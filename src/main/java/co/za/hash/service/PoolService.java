package co.za.hash.service;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

import org.apache.camel.Body;
import org.springframework.stereotype.Service;

import co.za.hash.config.ApplicationProperties;
import co.za.hash.domain.APool;
import co.za.hash.domain.Market;
import co.za.hash.domain.Order;
import co.za.hash.domain.nice.myorder.MyOrder;

@Service
public class PoolService {
	final Set<APool> pools = new HashSet<APool>();
	private Iterator<APool> iter;
	final ApplicationProperties props;
	//final MarketService market;
	
	public PoolService(ApplicationProperties props, MarketService marketService){
		props.getDpools().forEach(p->pools.add(new APool(p, marketService.get(p.getLocation(), p.getAlgo()))));
		this.props = props;
	}
	
	public APool getPool(MyOrder myOrder){
		APool orderPool = new APool(myOrder);
		//MPool pool = pools.stream().filter(p->p.getHost().equals(orderPool.getHost())).findFirst().orElse(orderPool);
		boolean isNew = pools.add(orderPool);
		
		APool retPool = isNew?orderPool:pools.stream().filter(p->p.equals(orderPool)).findFirst().get();
		return retPool;
		//return orderPool;
	}
	
	public static final String M_HAS_NEXT = "hasNext";
	public boolean hasNext(){
		if(iter == null){
			iter = pools.iterator();
		}
		boolean result = iter.hasNext();
		if(!result)iter = null;
		return result;
	}
	
	public static final String M_NEXT = "next";
	public APool next(){
		return iter.next();
	}
	
	
	public Optional<APool> getPool(String name){
		return pools.stream().filter(p->p.getName().equals(name)).findFirst();
	}
	
	
	
	public static final String M_GET_ALGO_MIN_SPEED = "getAlgoMinSpeed";
	public BigDecimal getAlgoMinSpeed(@Body Order order){
		return props.getNice().getAlgos().get(order.getPool().getAlgo()).getMinSpeed();
	}
	
	
	
}
