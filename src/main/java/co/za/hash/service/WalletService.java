package co.za.hash.service;

import java.math.BigDecimal;

import org.apache.camel.Body;
import org.springframework.stereotype.Service;

import co.za.hash.config.ApplicationProperties;
import co.za.hash.domain.Market;
import co.za.hash.domain.Order;

@Service
public class WalletService {
	private final ApplicationProperties props;
	public final static BigDecimal MIN_BTC = new BigDecimal("0.00000005");
	
	public WalletService(ApplicationProperties props) {
		super();
		this.props = props;
	}

	private BigDecimal btcAvailable = new BigDecimal("0");

	public BigDecimal getBtcAvailable() {
		return btcAvailable;
	}
	
	public static final String M_SET_BTC = "setBtcAvailable";
	public BigDecimal setBtcAvailable(@Body String btcAvailable) {
		this.btcAvailable = new BigDecimal(btcAvailable);
		return this.btcAvailable;
	}
	
	public void setBtcAvailable(@Body BigDecimal btcAvailable) {
		this.btcAvailable = btcAvailable;
	}
	
	public BigDecimal getBtcForOrder(Market market){
		BigDecimal amount = market.getAlgo().getAmount() == null?props.getNice().getOrder().getRefill():market.getAlgo().getAmount();
		
		if(amount.compareTo(btcAvailable) == -1){
			return amount;
		}else{
			return btcAvailable.subtract(MIN_BTC);
		}
	}
	
	public static final String M_GET_BTC_REFILL = "getBtcForRefill";
	public BigDecimal getBtcForRefill(){
		if(props.getNice().getOrder().getRefill().compareTo(btcAvailable) == -1){
			return props.getNice().getOrder().getRefill();
		}else{
			return btcAvailable.subtract(MIN_BTC);
		}
	}
	
	public static final String M_IS_WALLET_EMPTY = "isWalletEmpty";
	
	public boolean isWalletEmpty(){
		int compare = btcAvailable.compareTo(props.getNice().getOrder().getRefill());
		return compare <= 0;
	}
	
	public void doDecBtc(Order order){
		btcAvailable = btcAvailable.subtract(order.getBtcAvailable());
	}
	
	
}
