package co.za.hash.domain;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import co.za.hash.config.ApplicationProperties.Algo;
import co.za.hash.config.ApplicationProperties.MultiPool;
import co.za.hash.service.MultiPoolService;

public class Market {
	private Location loc;
	private BigDecimal minPrice;
	private BigDecimal maxPrice;
	private Algo algo;
	private Integer id;
	private BigDecimal minAdd;
	Map<MultiPool, BigDecimal> poolMaxPrices = new HashMap<MultiPool, BigDecimal>();
	
	
//	public Market(Location loc, BigDecimal price) {
//		super();
//		this.loc = loc;
//		this.price = price;
//	}
	public Market(){}
	
	public Market(Location loc, Algo algo) {
		super();
		this.loc = loc;
		this.setAlgo(algo);
		this.id = getId(loc.ordinal(), algo.getId());
	}


	public Location getLoc() {
		return loc;
	}
	public void setLoc(Location loc) {
		this.loc = loc;
	}
	public BigDecimal getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(BigDecimal price) {
		this.minPrice = price;
	}


	public Algo getAlgo() {
		return algo;
	}


	public void setAlgo(Algo algo) {
		this.algo = algo;
	}


	public Integer getId() {
		return id;
	}
	
//	public void setId(Integer id){
//		this.id = id;
//		this.loc = Location.setOrd((int)id/100);
//		this.algo =  id-100
//	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Market))
			return false;
		Market other = (Market) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	public static Integer getId(int location, int algo){
		return location*100+algo;
	}

	@Override
	public String toString() {
		return "Market [loc=" + loc + ", minPrice=" + minPrice + ", algo=" + algo + ", id=" + id +" maxPrice="+maxPrice+" multiMaxPrice="+poolMaxPrices+"]";
	}

	public BigDecimal getMinAdd() {
		return minAdd;
	}

	public void setMinAdd(BigDecimal minAdd) {
		this.minAdd = minAdd;
	}

	public BigDecimal getMaxPrice(MultiPool pool) {
		return poolMaxPrices.get(pool);
	}
	
	
	public void setMaxPrice(MultiPool pool, BigDecimal maxPrice) {
		this.poolMaxPrices.put(pool, maxPrice);
	}
	
	

	public BigDecimal getMaxPrice() {
		return maxPrice;
	}

	public static String SET_MAX_PRICE = "setMaxPrice(${header.maxPrice})";
	public void setMaxPrice(BigDecimal maxPrice) {
		this.maxPrice = maxPrice;
	}
	
	
}
