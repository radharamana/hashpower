package co.za.hash.domain.mpool;

import java.util.HashMap;

import org.apache.camel.Body;

public class YimpStatusC {

	private HashMap<String, YimpAlgoC> algos;

	public HashMap<String, YimpAlgoC> getAlgos() {
		return algos;
	}

	public void setAlgos(HashMap<String, YimpAlgoC> algos) {
		this.algos = algos;
	}

	public static final String M_DO_WRAP = "doWrap";
	public static String doWrap(@Body String jsonMap){
		return "{ \"algos\":"+jsonMap+"}";
	}
}