package co.za.hash.domain.mpool;

import java.util.HashMap;

import org.apache.camel.Body;

public class YimpStatusD {

	private HashMap<String, YimpAlgoD> algos;

	public HashMap<String, YimpAlgoD> getAlgos() {
		return algos;
	}

	public void setAlgos(HashMap<String, YimpAlgoD> algos) {
		this.algos = algos;
	}

	public static final String M_DO_WRAP = "doWrap";
	public static String doWrap(@Body String jsonMap){
		return "{ \"algos\":"+jsonMap+"}";
	}
}

