package co.za.hash.domain.mpool.hub;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "success", "return" })
public class HubStatus {

	@JsonProperty("success")
	private Boolean success;
	@JsonProperty("return")
	private List<Return> _return = null;

	@JsonProperty("success")
	public Boolean getSuccess() {
		return success;
	}

	@JsonProperty("success")
	public void setSuccess(Boolean success) {
		this.success = success;
	}

	@JsonProperty("return")
	public List<Return> getReturn() {
		return _return;
	}

	@JsonProperty("return")
	public void setReturn(List<Return> _return) {
		this._return = _return;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("success", success).append("_return", _return).toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(_return).append(success).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof HubStatus) == false) {
			return false;
		}
		HubStatus rhs = ((HubStatus) other);
		return new EqualsBuilder().append(_return, rhs._return).append(success, rhs.success).isEquals();
	}

}