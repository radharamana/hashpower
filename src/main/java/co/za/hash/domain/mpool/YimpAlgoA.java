package co.za.hash.domain.mpool;

import java.math.BigDecimal;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "port", "coins", "fees", "hashrate", "workers", "estimate_current", "estimate_last24h",
		"actual_last24h","mbtc_mh_factor", "estimate_current_in_btc_per_hash_per_day", "estimate_last24h_in_btc_per_hash_per_day",
		"actual_last24h_in_btc_per_hash_per_day", "hashrate_last24h","estimate_old","rental_current" })
public class YimpAlgoA {

	@JsonProperty("name")
	private String name;
	@JsonProperty("port")
	private Integer port;
	@JsonProperty("coins")
	private Integer coins;
	@JsonProperty("fees")
	private BigDecimal fees;
	@JsonProperty("hashrate")
	private Long hashrate;
	@JsonProperty("workers")
	private Integer workers;
	@JsonProperty("estimate_current")
	private BigDecimal estimateCurrent;
	@JsonProperty("estimate_last24h")
	private BigDecimal estimateLast24h;
	@JsonProperty("actual_last24h")
	private BigDecimal actualLast24h;
	
	@JsonProperty("mbtc_mh_factor")
	private BigDecimal mbtc_mh_factor;
	@JsonProperty("estimate_current_in_btc_per_hash_per_day")
	private BigDecimal estimateCurrentInBtcPerHashPerDay;
	@JsonProperty("estimate_last24h_in_btc_per_hash_per_day")
	private BigDecimal estimateLast24hInBtcPerHashPerDay;
	@JsonProperty("actual_last24h_in_btc_per_hash_per_day")
	private BigDecimal actualLast24hInBtcPerHashPerDay;
	@JsonProperty("hashrate_last24h")
	private Float hashrateLast24h;
	@JsonProperty("estimate_old")
	private BigDecimal estimate_old;
	@JsonProperty("rental_current")
	private BigDecimal rentalCurrent;
	
	

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("port")
	public Integer getPort() {
		return port;
	}

	@JsonProperty("port")
	public void setPort(Integer port) {
		this.port = port;
	}

	@JsonProperty("coins")
	public Integer getCoins() {
		return coins;
	}

	@JsonProperty("coins")
	public void setCoins(Integer coins) {
		this.coins = coins;
	}

	@JsonProperty("fees")
	public BigDecimal getFees() {
		return fees;
	}

	@JsonProperty("fees")
	public void setFees(BigDecimal fees) {
		this.fees = fees;
	}

	@JsonProperty("hashrate")
	public Long getHashrate() {
		return hashrate;
	}

	@JsonProperty("hashrate")
	public void setHashrate(Long hashrate) {
		this.hashrate = hashrate;
	}

	@JsonProperty("workers")
	public Integer getWorkers() {
		return workers;
	}

	@JsonProperty("workers")
	public void setWorkers(Integer workers) {
		this.workers = workers;
	}

	@JsonProperty("estimate_current")
	public BigDecimal getEstimateCurrent() {
		return estimateCurrent;
	}

	@JsonProperty("estimate_current")
	public void setEstimateCurrent(BigDecimal estimateCurrent) {
		this.estimateCurrent = estimateCurrent;
	}

	@JsonProperty("estimate_last24h")
	public BigDecimal getEstimateLast24h() {
		return estimateLast24h;
	}

	@JsonProperty("estimate_last24h")
	public void setEstimateLast24h(BigDecimal estimateLast24h) {
		this.estimateLast24h = estimateLast24h;
	}

	@JsonProperty("actual_last24h")
	public BigDecimal getActualLast24h() {
		return actualLast24h;
	}

	@JsonProperty("actual_last24h")
	public void setActualLast24h(BigDecimal actualLast24h) {
		this.actualLast24h = actualLast24h;
	}

	@JsonProperty("hashrate_last24h")
	public Float getHashrateLast24h() {
		return hashrateLast24h;
	}

	@JsonProperty("hashrate_last24h")
	public void setHashrateLast24h(Float hashrateLast24h) {
		this.hashrateLast24h = hashrateLast24h;
	}

	public BigDecimal getRentalCurrent() {
		return rentalCurrent;
	}

	public void setRentalCurrent(BigDecimal rentalCurrent) {
		this.rentalCurrent = rentalCurrent;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("port", port).append("coins", coins)
				.append("fees", fees).append("hashrate", hashrate).append("workers", workers)
				.append("estimateCurrent", estimateCurrent).append("estimateLast24h", estimateLast24h)
				.append("actualLast24h", actualLast24h).append("hashrateLast24h", hashrateLast24h).toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(port).append(hashrateLast24h).append(estimateCurrent).append(coins)
				.append(estimateLast24h).append(name).append(hashrate).append(fees).append(workers)
				.append(actualLast24h).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof YimpAlgoA) == false) {
			return false;
		}
		YimpAlgoA rhs = ((YimpAlgoA) other);
		return new EqualsBuilder().append(port, rhs.port).append(hashrateLast24h, rhs.hashrateLast24h)
				.append(estimateCurrent, rhs.estimateCurrent).append(coins, rhs.coins)
				.append(estimateLast24h, rhs.estimateLast24h).append(name, rhs.name).append(hashrate, rhs.hashrate)
				.append(fees, rhs.fees).append(workers, rhs.workers).append(actualLast24h, rhs.actualLast24h)
				.isEquals();
	}

}