package co.za.hash.domain.mpool;

import java.util.HashMap;

import org.apache.camel.Body;

public class YimpStatus {

	private HashMap<String, YimpAlgoA> algos;

	public HashMap<String, YimpAlgoA> getAlgos() {
		return algos;
	}

	public void setAlgos(HashMap<String, YimpAlgoA> algos) {
		this.algos = algos;
	}

	public static final String M_DO_WRAP = "doWrap";
	public static String doWrap(@Body String jsonMap){
		return "{ \"algos\":"+jsonMap+"}";
	}
}