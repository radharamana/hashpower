package co.za.hash.domain.mpool;

import java.util.HashMap;

import org.apache.camel.Body;

import co.za.hash.domain.mpool.YimpAlgoB;



public class YimpStatusB {

	private HashMap<String, YimpAlgoB> algos;

	public HashMap<String, YimpAlgoB> getAlgos() {
		return algos;
	}

	public void setAlgos(HashMap<String, YimpAlgoB> algos) {
		this.algos = algos;
	}

	public static final String M_DO_WRAP = "doWrap";
	public static String doWrap(@Body String jsonMap){
		return "{ \"algos\":"+jsonMap+"}";
	}
}