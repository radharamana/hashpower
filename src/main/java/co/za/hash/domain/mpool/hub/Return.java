package co.za.hash.domain.mpool.hub;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "algo", "current_mining_coin", "host", "all_host_list", "port", "algo_switch_port",
		"multialgo_switch_port", "profit", "normalized_profit_amd", "normalized_profit_nvidia" })
public class Return {

	@JsonProperty("algo")
	private String algo;
	@JsonProperty("current_mining_coin")
	private String currentMiningCoin;
	@JsonProperty("host")
	private String host;
	@JsonProperty("all_host_list")
	private String allHostList;
	@JsonProperty("port")
	private Integer port;
	@JsonProperty("algo_switch_port")
	private Integer algoSwitchPort;
	@JsonProperty("multialgo_switch_port")
	private Integer multialgoSwitchPort;
	@JsonProperty("profit")
	private BigDecimal profit;
	@JsonProperty("normalized_profit_amd")
	private Float normalizedProfitAmd;
	@JsonProperty("normalized_profit_nvidia")
	private Float normalizedProfitNvidia;

	@JsonProperty("algo")
	public String getAlgo() {
		return algo;
	}

	@JsonProperty("algo")
	public void setAlgo(String algo) {
		this.algo = algo;
	}

	@JsonProperty("current_mining_coin")
	public String getCurrentMiningCoin() {
		return currentMiningCoin;
	}

	@JsonProperty("current_mining_coin")
	public void setCurrentMiningCoin(String currentMiningCoin) {
		this.currentMiningCoin = currentMiningCoin;
	}

	@JsonProperty("host")
	public String getHost() {
		return host;
	}

	@JsonProperty("host")
	public void setHost(String host) {
		this.host = host;
	}

	@JsonProperty("all_host_list")
	public String getAllHostList() {
		return allHostList;
	}

	@JsonProperty("all_host_list")
	public void setAllHostList(String allHostList) {
		this.allHostList = allHostList;
	}

	@JsonProperty("port")
	public Integer getPort() {
		return port;
	}

	@JsonProperty("port")
	public void setPort(Integer port) {
		this.port = port;
	}

	@JsonProperty("algo_switch_port")
	public Integer getAlgoSwitchPort() {
		return algoSwitchPort;
	}

	@JsonProperty("algo_switch_port")
	public void setAlgoSwitchPort(Integer algoSwitchPort) {
		this.algoSwitchPort = algoSwitchPort;
	}

	@JsonProperty("multialgo_switch_port")
	public Integer getMultialgoSwitchPort() {
		return multialgoSwitchPort;
	}

	@JsonProperty("multialgo_switch_port")
	public void setMultialgoSwitchPort(Integer multialgoSwitchPort) {
		this.multialgoSwitchPort = multialgoSwitchPort;
	}

	@JsonProperty("profit")
	public BigDecimal getProfit() {
		return profit;
	}

	@JsonProperty("profit")
	public void setProfit(BigDecimal profit) {
		this.profit = profit;
	}

	@JsonProperty("normalized_profit_amd")
	public Float getNormalizedProfitAmd() {
		return normalizedProfitAmd;
	}

	@JsonProperty("normalized_profit_amd")
	public void setNormalizedProfitAmd(Float normalizedProfitAmd) {
		this.normalizedProfitAmd = normalizedProfitAmd;
	}

	@JsonProperty("normalized_profit_nvidia")
	public Float getNormalizedProfitNvidia() {
		return normalizedProfitNvidia;
	}

	@JsonProperty("normalized_profit_nvidia")
	public void setNormalizedProfitNvidia(Float normalizedProfitNvidia) {
		this.normalizedProfitNvidia = normalizedProfitNvidia;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("algo", algo).append("currentMiningCoin", currentMiningCoin)
				.append("host", host).append("allHostList", allHostList).append("port", port)
				.append("algoSwitchPort", algoSwitchPort).append("multialgoSwitchPort", multialgoSwitchPort)
				.append("profit", profit).append("normalizedProfitAmd", normalizedProfitAmd)
				.append("normalizedProfitNvidia", normalizedProfitNvidia).toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(port).append(profit).append(algoSwitchPort).append(host).append(algo)
				.append(normalizedProfitAmd).append(multialgoSwitchPort).append(allHostList).append(currentMiningCoin)
				.append(normalizedProfitNvidia).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Return) == false) {
			return false;
		}
		Return rhs = ((Return) other);
		return new EqualsBuilder().append(port, rhs.port).append(profit, rhs.profit)
				.append(algoSwitchPort, rhs.algoSwitchPort).append(host, rhs.host).append(algo, rhs.algo)
				.append(normalizedProfitAmd, rhs.normalizedProfitAmd)
				.append(multialgoSwitchPort, rhs.multialgoSwitchPort).append(allHostList, rhs.allHostList)
				.append(currentMiningCoin, rhs.currentMiningCoin)
				.append(normalizedProfitNvidia, rhs.normalizedProfitNvidia).isEquals();
	}

}