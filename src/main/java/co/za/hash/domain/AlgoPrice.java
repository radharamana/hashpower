package co.za.hash.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import com.google.common.collect.Ordering;

import co.za.hash.config.ApplicationProperties;
import co.za.hash.config.ApplicationProperties.Algo;
import co.za.hash.config.ApplicationProperties.MultiPool;
import co.za.hash.util.ValueComparableMap;

public class AlgoPrice implements Comparable<AlgoPrice> {
	final ApplicationProperties props;
	final Algo algo;
	public AlgoPrice(ApplicationProperties props, Algo algo){
		this.props = props;
		this.algo = algo;
	}
	
	
	
	BigDecimal niceHashPrice;
	
	TreeMap<MultiPool, BigDecimal> multiPrices = new ValueComparableMap<MultiPool, BigDecimal>(Ordering.natural());
	//TreeMap<MultiPool, BigDecimal> multiPricesOrd = new TreeMap<MultiPool, BigDecimal>(bvc);
	HashMap<MultiPool, BigDecimal> poolFees = new HashMap<MultiPool, BigDecimal>();
	
	
	public Algo getAlgo() {
		return algo;
	}
	
	
	public BigDecimal getNiceHashPrice() {
		return niceHashPrice;
	}
	public void setNiceHashPrice(BigDecimal niceHashPrice) {
		this.niceHashPrice = niceHashPrice;
	}
//	public Map<MultiPool, BigDecimal> getMultiPrices() {
//		return multiPrices;
//	}
//	public void setMultiPrices(Map<MultiPool, BigDecimal> multiPrices) {
//		this.multiPrices = multiPrices;
//	}
	
	public void addMultiPrice(MultiPool pool, BigDecimal price, BigDecimal fee, Algo algo){
		
		BigDecimal newPrice = price.multiply(algo.getAdj());
		if(newPrice.compareTo(BigDecimal.ZERO)!=0){
			while(niceHashPrice.divide(newPrice,1, RoundingMode.HALF_EVEN).compareTo(BigDecimal.TEN) > 0){
				newPrice = newPrice.multiply(BigDecimal.TEN);
			}
			while(niceHashPrice.divide(newPrice,3, RoundingMode.HALF_EVEN).compareTo(new BigDecimal("0.5")) < 0){
				newPrice = newPrice.divide(BigDecimal.TEN);
			}
		}
		this.multiPrices.put(pool, newPrice);
		this.poolFees.put(pool, fee);
	}
	
	public BigDecimal getProfitPercent(){
		if(niceHashPrice == null || multiPrices.lastEntry() == null || BigDecimal.ZERO.equals(niceHashPrice))return BigDecimal.ZERO;
		MultiPool mp = multiPrices.lastEntry().getKey();
		return multiPrices.lastEntry().getValue()
				.subtract(niceHashPrice)
				.divide(niceHashPrice, 8, RoundingMode.HALF_UP)
				.multiply(BigDecimal.valueOf(100l))
				.subtract(props.getNice().getFee())
				.subtract(poolFees.get(mp))
				.subtract(mp.getFee())
				.subtract(props.getRejectPercent());
	}
	
	public BigDecimal getProfitPrice(MultiPool pool, BigDecimal percent){
		if(multiPrices.lastEntry() == null)return BigDecimal.ZERO;
		BigDecimal price = multiPrices.get(pool);
		BigDecimal fullPer = percent.add(props.getNice().getFee()).add(poolFees.get(pool)).add(pool.getFee()).add(props.getRejectPercent());
		BigDecimal per = BigDecimal.ONE.subtract(fullPer.divide(BigDecimal.valueOf(100l)));
		return price.multiply(per);
	}
	
	@Override
	public int compareTo(AlgoPrice o) {
		return this.getProfitPercent().compareTo(o.getProfitPercent()) * -1;
	}


	@Override
	public String toString() {
		return "AlgoPrice [algo=" + algo + ", niceHashPrice=" + niceHashPrice + ", multiPrices=" + multiPrices
				+ ", poolFees=" + poolFees + ", profitPercant:"+getProfitPercent()+" bestPool:"+getBestPool()+" bestPrice:"+getBestPrice()+"]";
	}
	
	public String toSummaryString(){
		return "Algo:"+algo+" Best Pool:"+getBestPool()+" Best Percent:"+getProfitPercent()+" Best Price:"+getBestPrice()+" Nice Hash Price:"+niceHashPrice;
	}
	
	public MultiPool getBestPool(){
		if(multiPrices.size() == 0)return null;
		return multiPrices.lastEntry().getKey();
	}
	
	public BigDecimal getBestPrice(){
		if(multiPrices.size() == 0)return BigDecimal.ZERO;
		return multiPrices.lastEntry().getValue();
	}


	public Set<MultiPool> getPools() {
		return multiPrices.keySet();
	}
	
//	@Override
//	public String toString(){
//		if(multiPrices.size() == 0)return "0";
//		return multiPrices.lastEntry().toString();
//	}
	
	
	
}


