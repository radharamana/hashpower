package co.za.hash.domain;

import co.za.hash.util.CamelEnum;

public enum PoolFormat implements CamelEnum{
	YIIMP, HUB, YIIMPC, YIIMPB;

	@Override
	public String getCamelType() {
		String type = "${type:"+this.getClass().getName()+"."+this.name()+"}";
		return type;
	}
}
