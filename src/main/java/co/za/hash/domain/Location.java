package co.za.hash.domain;

import java.util.Arrays;
import java.util.List;

public enum Location {
	EU("europe"),US("us-east");
	
	private List<String> altNames;
	
	private Location(String ... altNames){
		this.setAltNames(Arrays.asList(altNames));
	}
	
	public static Location setOrd(int id){
		switch(id){
		case 0:return Location.EU;
		case 1:return Location.US;
		}
		return null;
	}

	public List<String> getAltNames() {
		return altNames;
	}

	public void setAltNames(List<String> altNames) {
		this.altNames = altNames;
	}
	
	
}
