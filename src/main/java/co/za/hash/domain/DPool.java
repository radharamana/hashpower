package co.za.hash.domain;

import java.math.BigDecimal;

public class DPool {
	protected String name;
	private int delay;
	private int period;
	private String host;
	private String worker;
	private String pass;
	private int port;
	private int algo;
	private int location;
	private BigDecimal limitSpeed;
	private Integer blockMins;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getDelay() {
		return delay;
	}

	public void setDelay(int delay) {
		this.delay = delay;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getWorker() {
		return worker;
	}

	public void setWorker(String worker) {
		this.worker = worker;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getAlgo() {
		return algo;
	}

	public void setAlgo(int algo) {
		this.algo = algo;
	}

	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

	

	public Integer getBlockMins() {
		return blockMins;
	}

	public void setBlockMins(Integer blockMins) {
		this.blockMins = blockMins;
	}

	public BigDecimal getLimitSpeed() {
		return limitSpeed;
	}

	public void setLimitSpeed(BigDecimal limitSpeed) {
		this.limitSpeed = limitSpeed;
	}
}
