package co.za.hash.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "is_mature", "date_found", "hash", "confirmations", "total_shares", "total_score", "reward",
		"mining_duration", "date_started", "nmc_reward" })
public class SlushBlock {

	@JsonProperty("is_mature")
	private Integer isMature;
	@JsonProperty("date_found")
	private String dateFound;
	@JsonProperty("hash")
	private String hash;
	@JsonProperty("confirmations")
	private Integer confirmations;
	@JsonProperty("total_shares")
	private Long totalShares;
	@JsonProperty("total_score")
	private Double totalScore;
	@JsonProperty("reward")
	private String reward;
	@JsonProperty("mining_duration")
	private Double miningDuration;
	@JsonProperty("date_started")
	private String dateStarted;
	@JsonProperty("nmc_reward")
	private String nmcReward;

	@JsonProperty("is_mature")
	public Integer getIsMature() {
		return isMature;
	}

	@JsonProperty("is_mature")
	public void setIsMature(Integer isMature) {
		this.isMature = isMature;
	}

	@JsonProperty("date_found")
	public String getDateFound() {
		return dateFound;
	}

	@JsonProperty("date_found")
	public void setDateFound(String dateFound) {
		this.dateFound = dateFound;
	}

	@JsonProperty("hash")
	public String getHash() {
		return hash;
	}

	@JsonProperty("hash")
	public void setHash(String hash) {
		this.hash = hash;
	}

	@JsonProperty("confirmations")
	public Integer getConfirmations() {
		return confirmations;
	}

	@JsonProperty("confirmations")
	public void setConfirmations(Integer confirmations) {
		this.confirmations = confirmations;
	}

	@JsonProperty("total_shares")
	public Long getTotalShares() {
		return totalShares;
	}

	@JsonProperty("total_shares")
	public void setTotalShares(Long totalShares) {
		this.totalShares = totalShares;
	}

	@JsonProperty("total_score")
	public Double getTotalScore() {
		return totalScore;
	}

	@JsonProperty("total_score")
	public void setTotalScore(Double totalScore) {
		this.totalScore = totalScore;
	}

	@JsonProperty("reward")
	public String getReward() {
		return reward;
	}

	@JsonProperty("reward")
	public void setReward(String reward) {
		this.reward = reward;
	}

	@JsonProperty("mining_duration")
	public Double getMiningDuration() {
		return miningDuration;
	}

	@JsonProperty("mining_duration")
	public void setMiningDuration(Double miningDuration) {
		this.miningDuration = miningDuration;
	}

	@JsonProperty("date_started")
	public String getDateStarted() {
		return dateStarted;
	}

	@JsonProperty("date_started")
	public void setDateStarted(String dateStarted) {
		this.dateStarted = dateStarted;
	}

	@JsonProperty("nmc_reward")
	public String getNmcReward() {
		return nmcReward;
	}

	@JsonProperty("nmc_reward")
	public void setNmcReward(String nmcReward) {
		this.nmcReward = nmcReward;
	}

}