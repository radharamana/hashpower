package co.za.hash.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import co.za.hash.config.ApplicationProperties.Algo;
import co.za.hash.config.ApplicationProperties.MultiPool;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "limit_speed", "alive", "price", "id", "type", "workers", "algo", "accepted_speed","btc_avail" })
@JsonIgnoreProperties(value="algo")
public class Order {
	
		
	
	private APool pool;
	private Market market;

	@JsonProperty("limit_speed")
	private BigDecimal limitSpeed;
	@JsonProperty("alive")
	private Boolean alive;
	@JsonProperty("price")
	private BigDecimal price;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("type")
	private Integer type;
	@JsonProperty("workers")
	private Integer workers;
//	@JsonProperty("algo")
//	private Integer algo;
	@JsonProperty("accepted_speed")
	private float acceptedSpeed;
	@JsonProperty("btc_avail")
	private BigDecimal btcAvailable;

	private boolean isExpired = false;
	
	
	@JsonProperty("limit_speed")
	public BigDecimal getLimitSpeed() {
		return limitSpeed.setScale(2, RoundingMode.DOWN);
	}

	@JsonProperty("limit_speed")
	public Order setLimitSpeed(BigDecimal limitSpeed) {
		this.limitSpeed = limitSpeed;
		return this;
	}
	
	
	
	@JsonProperty("alive")
	public Boolean getAlive() {
		return alive;
	}

	@JsonProperty("alive")
	public Order setAlive(Boolean alive) {
		this.alive = alive;
		return this;
	}

	@JsonProperty("price")
	public BigDecimal getPrice() {
		return price.setScale(4, RoundingMode.DOWN);
	}
	
	
	@JsonProperty("price")
	public Order setPrice(BigDecimal price) {
		this.price = price;
		return this;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public Order setId(Integer id) {
		this.id = id;
		return this;
	}

	@JsonProperty("type")
	public Integer getType() {
		return type;
	}

	@JsonProperty("type")
	public Order setType(Integer type) {
		this.type = type;
		return this;
	}

	@JsonProperty("workers")
	public Integer getWorkers() {
		return workers;
	}

	@JsonProperty("workers")
	public Order setWorkers(Integer workers) {
		this.workers = workers;
		return this;
	}

//	@JsonProperty("algo")
//	public Integer getAlgo() {
//		return algo;
//	}
//
//	@JsonProperty("algo")
//	public Order setAlgo(Integer algo) {
//		this.algo = algo;
//		return this;
//	}

	@JsonProperty("accepted_speed")
	public float getAcceptedSpeed() {
		return acceptedSpeed;
	}

	@JsonProperty("accepted_speed")
	public Order setAcceptedSpeed(float acceptedSpeed) {
		this.acceptedSpeed = acceptedSpeed;
		return this;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Order))
			return false;
		Order other = (Order) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public APool getPool() {
		return pool;
	}

	public Order setPool(APool pool) {
		this.pool = pool;
		return this;
	}

	
	@Override
	public String toString() {
		return "Order [pool=" + pool + ", limitSpeed=" + limitSpeed + ", alive=" + alive + ", price=" + price + ", id="
				+ id + ", type=" + type + ", workers=" + workers + ", acceptedSpeed=" + acceptedSpeed
				+ ", btcAvailable=" + btcAvailable + ", isExpired="+isExpired +" market="+market+"]";
	}

	public BigDecimal getBtcAvailable() {
		return btcAvailable;
	}

	public Order setBtcAvailable(BigDecimal btcAvailable) {
		this.btcAvailable = btcAvailable;
		return this;
	}

	public boolean isExpired() {
		return isExpired;
	}

	public void setExpired(boolean isExpired) {
		this.isExpired = isExpired;
	}

	public Market getMarket() {
		return market;
	}

	public Order setMarket(Market market) {
		this.market = market;
		return this;
	}

	
	
	
	
	

}