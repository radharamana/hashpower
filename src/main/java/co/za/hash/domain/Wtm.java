package co.za.hash.domain;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "tag", "algorithm", "block_time", "block_reward", "block_reward24", "block_reward3",
		"block_reward7", "last_block", "difficulty", "difficulty24", "difficulty3", "difficulty7", "nethash",
		"exchange_rate", "exchange_rate24", "exchange_rate3", "exchange_rate7", "exchange_rate_vol",
		"exchange_rate_curr", "market_cap", "pool_fee", "estimated_rewards", "btc_revenue", "revenue", "cost", "profit",
		"status", "lagging", "testing", "listed", "timestamp" })
public class Wtm {

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("tag")
	private String tag;
	@JsonProperty("algorithm")
	private String algorithm;
	@JsonProperty("block_time")
	private String blockTime;
	@JsonProperty("block_reward")
	private Double blockReward;
	@JsonProperty("block_reward24")
	private Double blockReward24;
	@JsonProperty("block_reward3")
	private Double blockReward3;
	@JsonProperty("block_reward7")
	private Double blockReward7;
	@JsonProperty("last_block")
	private Integer lastBlock;
	@JsonProperty("difficulty")
	private Double difficulty;
	@JsonProperty("difficulty24")
	private Double difficulty24;
	@JsonProperty("difficulty3")
	private Double difficulty3;
	@JsonProperty("difficulty7")
	private Double difficulty7;
	@JsonProperty("nethash")
	private String nethash;
	@JsonProperty("exchange_rate")
	private Double exchangeRate;
	@JsonProperty("exchange_rate24")
	private Double exchangeRate24;
	@JsonProperty("exchange_rate3")
	private Double exchangeRate3;
	@JsonProperty("exchange_rate7")
	private Double exchangeRate7;
	@JsonProperty("exchange_rate_vol")
	private Double exchangeRateVol;
	@JsonProperty("exchange_rate_curr")
	private String exchangeRateCurr;
	@JsonProperty("market_cap")
	private String marketCap;
	@JsonProperty("pool_fee")
	private String poolFee;
	@JsonProperty("estimated_rewards")
	private String estimatedRewards;
	@JsonProperty("btc_revenue")
	private BigDecimal btcRevenue;
	@JsonProperty("revenue")
	private String revenue;
	@JsonProperty("cost")
	private String cost;
	@JsonProperty("profit")
	private String profit;
	@JsonProperty("status")
	private String status;
	@JsonProperty("lagging")
	private Boolean lagging;
	@JsonProperty("testing")
	private Boolean testing;
	@JsonProperty("listed")
	private Boolean listed;
	@JsonProperty("timestamp")
	private Integer timestamp;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("tag")
	public String getTag() {
		return tag;
	}

	@JsonProperty("tag")
	public void setTag(String tag) {
		this.tag = tag;
	}

	@JsonProperty("algorithm")
	public String getAlgorithm() {
		return algorithm;
	}

	@JsonProperty("algorithm")
	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	@JsonProperty("block_time")
	public String getBlockTime() {
		return blockTime;
	}

	@JsonProperty("block_time")
	public void setBlockTime(String blockTime) {
		this.blockTime = blockTime;
	}

	@JsonProperty("block_reward")
	public Double getBlockReward() {
		return blockReward;
	}

	@JsonProperty("block_reward")
	public void setBlockReward(Double blockReward) {
		this.blockReward = blockReward;
	}

	@JsonProperty("block_reward24")
	public Double getBlockReward24() {
		return blockReward24;
	}

	@JsonProperty("block_reward24")
	public void setBlockReward24(Double blockReward24) {
		this.blockReward24 = blockReward24;
	}

	@JsonProperty("block_reward3")
	public Double getBlockReward3() {
		return blockReward3;
	}

	@JsonProperty("block_reward3")
	public void setBlockReward3(Double blockReward3) {
		this.blockReward3 = blockReward3;
	}

	@JsonProperty("block_reward7")
	public Double getBlockReward7() {
		return blockReward7;
	}

	@JsonProperty("block_reward7")
	public void setBlockReward7(Double blockReward7) {
		this.blockReward7 = blockReward7;
	}

	@JsonProperty("last_block")
	public Integer getLastBlock() {
		return lastBlock;
	}

	@JsonProperty("last_block")
	public void setLastBlock(Integer lastBlock) {
		this.lastBlock = lastBlock;
	}

	@JsonProperty("difficulty")
	public Double getDifficulty() {
		return difficulty;
	}

	@JsonProperty("difficulty")
	public void setDifficulty(Double difficulty) {
		this.difficulty = difficulty;
	}

	@JsonProperty("difficulty24")
	public Double getDifficulty24() {
		return difficulty24;
	}

	@JsonProperty("difficulty24")
	public void setDifficulty24(Double difficulty24) {
		this.difficulty24 = difficulty24;
	}

	@JsonProperty("difficulty3")
	public Double getDifficulty3() {
		return difficulty3;
	}

	@JsonProperty("difficulty3")
	public void setDifficulty3(Double difficulty3) {
		this.difficulty3 = difficulty3;
	}

	@JsonProperty("difficulty7")
	public Double getDifficulty7() {
		return difficulty7;
	}

	@JsonProperty("difficulty7")
	public void setDifficulty7(Double difficulty7) {
		this.difficulty7 = difficulty7;
	}

	@JsonProperty("nethash")
	public String getNethash() {
		return nethash;
	}

	@JsonProperty("nethash")
	public void setNethash(String nethash) {
		this.nethash = nethash;
	}

	@JsonProperty("exchange_rate")
	public Double getExchangeRate() {
		return exchangeRate;
	}

	@JsonProperty("exchange_rate")
	public void setExchangeRate(Double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	@JsonProperty("exchange_rate24")
	public Double getExchangeRate24() {
		return exchangeRate24;
	}

	@JsonProperty("exchange_rate24")
	public void setExchangeRate24(Double exchangeRate24) {
		this.exchangeRate24 = exchangeRate24;
	}

	@JsonProperty("exchange_rate3")
	public Double getExchangeRate3() {
		return exchangeRate3;
	}

	@JsonProperty("exchange_rate3")
	public void setExchangeRate3(Double exchangeRate3) {
		this.exchangeRate3 = exchangeRate3;
	}

	@JsonProperty("exchange_rate7")
	public Double getExchangeRate7() {
		return exchangeRate7;
	}

	@JsonProperty("exchange_rate7")
	public void setExchangeRate7(Double exchangeRate7) {
		this.exchangeRate7 = exchangeRate7;
	}

	@JsonProperty("exchange_rate_vol")
	public Double getExchangeRateVol() {
		return exchangeRateVol;
	}

	@JsonProperty("exchange_rate_vol")
	public void setExchangeRateVol(Double exchangeRateVol) {
		this.exchangeRateVol = exchangeRateVol;
	}

	@JsonProperty("exchange_rate_curr")
	public String getExchangeRateCurr() {
		return exchangeRateCurr;
	}

	@JsonProperty("exchange_rate_curr")
	public void setExchangeRateCurr(String exchangeRateCurr) {
		this.exchangeRateCurr = exchangeRateCurr;
	}

	@JsonProperty("market_cap")
	public String getMarketCap() {
		return marketCap;
	}

	@JsonProperty("market_cap")
	public void setMarketCap(String marketCap) {
		this.marketCap = marketCap;
	}

	@JsonProperty("pool_fee")
	public String getPoolFee() {
		return poolFee;
	}

	@JsonProperty("pool_fee")
	public void setPoolFee(String poolFee) {
		this.poolFee = poolFee;
	}

	@JsonProperty("estimated_rewards")
	public String getEstimatedRewards() {
		return estimatedRewards;
	}

	@JsonProperty("estimated_rewards")
	public void setEstimatedRewards(String estimatedRewards) {
		this.estimatedRewards = estimatedRewards;
	}

	@JsonProperty("btc_revenue")
	public BigDecimal getBtcRevenue() {
		return btcRevenue;
	}

	@JsonProperty("btc_revenue")
	public void setBtcRevenue(BigDecimal btcRevenue) {
		this.btcRevenue = btcRevenue;
	}

	@JsonProperty("revenue")
	public String getRevenue() {
		return revenue;
	}

	@JsonProperty("revenue")
	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}

	@JsonProperty("cost")
	public String getCost() {
		return cost;
	}

	@JsonProperty("cost")
	public void setCost(String cost) {
		this.cost = cost;
	}

	@JsonProperty("profit")
	public String getProfit() {
		return profit;
	}

	@JsonProperty("profit")
	public void setProfit(String profit) {
		this.profit = profit;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("lagging")
	public Boolean getLagging() {
		return lagging;
	}

	@JsonProperty("lagging")
	public void setLagging(Boolean lagging) {
		this.lagging = lagging;
	}

	@JsonProperty("testing")
	public Boolean getTesting() {
		return testing;
	}

	@JsonProperty("testing")
	public void setTesting(Boolean testing) {
		this.testing = testing;
	}

	@JsonProperty("listed")
	public Boolean getListed() {
		return listed;
	}

	@JsonProperty("listed")
	public void setListed(Boolean listed) {
		this.listed = listed;
	}

	@JsonProperty("timestamp")
	public Integer getTimestamp() {
		return timestamp;
	}

	@JsonProperty("timestamp")
	public void setTimestamp(Integer timestamp) {
		this.timestamp = timestamp;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}