package co.za.hash.domain.nice.global;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "result", "method" })
public class Stat {

	@JsonProperty("result")
	private Result result;
	@JsonProperty("method")
	private String method;

	@JsonProperty("result")
	public Result getResult() {
		return result;
	}

	@JsonProperty("result")
	public void setResult(Result result) {
		this.result = result;
	}

	@JsonProperty("method")
	public String getMethod() {
		return method;
	}

	@JsonProperty("method")
	public void setMethod(String method) {
		this.method = method;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(result).append(method).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Stat) == false) {
			return false;
		}
		Stat rhs = ((Stat) other);
		return new EqualsBuilder().append(result, rhs.result).append(method, rhs.method).isEquals();
	}

}