package co.za.hash.domain.nice.market;

import java.time.Instant;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import co.za.hash.domain.Order;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "orders","timestamp" })
public class Result {

	@JsonProperty("orders")
	private List<Order> orders = null;
	
	private String timestamp;

	@JsonProperty("orders")
	public List<Order> getOrders() {
		return orders;
	}

	@JsonProperty("orders")
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	

}