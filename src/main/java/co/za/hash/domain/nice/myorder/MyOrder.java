package co.za.hash.domain.nice.myorder;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "type", "btc_avail", "limit_speed", "pool_user", "pool_port", "alive", "workers", "pool_pass",
		"accepted_speed", "id", "algo", "price", "btc_paid", "pool_host", "end" })
public class MyOrder {

	@JsonProperty("type")
	private Integer type;
	@JsonProperty("btc_avail")
	private BigDecimal btcAvail;
	@JsonProperty("limit_speed")
	private BigDecimal limitSpeed;
	@JsonProperty("pool_user")
	private String poolUser;
	@JsonProperty("pool_port")
	private Integer poolPort;
	@JsonProperty("alive")
	private Boolean alive;
	@JsonProperty("workers")
	private Integer workers;
	@JsonProperty("pool_pass")
	private String poolPass;
	@JsonProperty("accepted_speed")
	private Float acceptedSpeed;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("algo")
	private Integer algo;
	@JsonProperty("price")
	private BigDecimal price;
	@JsonProperty("btc_paid")
	private BigDecimal btcPaid;
	@JsonProperty("pool_host")
	private String poolHost;
	@JsonProperty("end")
	private Long end;

	@JsonProperty("type")
	public Integer getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(Integer type) {
		this.type = type;
	}

	@JsonProperty("btc_avail")
	public BigDecimal getBtcAvail() {
		return btcAvail;
	}

	@JsonProperty("btc_avail")
	public void setBtcAvail(BigDecimal btcAvail) {
		this.btcAvail = btcAvail;
	}

	@JsonProperty("limit_speed")
	public BigDecimal getLimitSpeed() {
		return limitSpeed;
	}

	@JsonProperty("limit_speed")
	public void setLimitSpeed(BigDecimal limitSpeed) {
		this.limitSpeed = limitSpeed;
	}

	@JsonProperty("pool_user")
	public String getPoolUser() {
		return poolUser;
	}

	@JsonProperty("pool_user")
	public void setPoolUser(String poolUser) {
		this.poolUser = poolUser;
	}

	@JsonProperty("pool_port")
	public Integer getPoolPort() {
		return poolPort;
	}

	@JsonProperty("pool_port")
	public void setPoolPort(Integer poolPort) {
		this.poolPort = poolPort;
	}

	@JsonProperty("alive")
	public Boolean getAlive() {
		return alive;
	}

	@JsonProperty("alive")
	public void setAlive(Boolean alive) {
		this.alive = alive;
	}

	@JsonProperty("workers")
	public Integer getWorkers() {
		return workers;
	}

	@JsonProperty("workers")
	public void setWorkers(Integer workers) {
		this.workers = workers;
	}

	@JsonProperty("pool_pass")
	public String getPoolPass() {
		return poolPass;
	}

	@JsonProperty("pool_pass")
	public void setPoolPass(String poolPass) {
		this.poolPass = poolPass;
	}

	@JsonProperty("accepted_speed")
	public Float getAcceptedSpeed() {
		return acceptedSpeed;
	}

	@JsonProperty("accepted_speed")
	public void setAcceptedSpeed(Float acceptedSpeed) {
		this.acceptedSpeed = acceptedSpeed;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("algo")
	public Integer getAlgo() {
		return algo;
	}

	@JsonProperty("algo")
	public void setAlgo(Integer algo) {
		this.algo = algo;
	}

	@JsonProperty("price")
	public BigDecimal getPrice() {
		return price;
	}

	@JsonProperty("price")
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@JsonProperty("btc_paid")
	public BigDecimal getBtcPaid() {
		return btcPaid;
	}

	@JsonProperty("btc_paid")
	public void setBtcPaid(BigDecimal btcPaid) {
		this.btcPaid = btcPaid;
	}

	@JsonProperty("pool_host")
	public String getPoolHost() {
		return poolHost;
	}

	@JsonProperty("pool_host")
	public void setPoolHost(String poolHost) {
		this.poolHost = poolHost;
	}

	@JsonProperty("end")
	public Long getEnd() {
		return end;
	}

	@JsonProperty("end")
	public void setEnd(Long end) {
		this.end = end;
	}

}