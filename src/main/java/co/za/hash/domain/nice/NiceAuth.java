package co.za.hash.domain.nice;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base32;
import org.springframework.stereotype.Component;

import co.za.hash.config.ApplicationProperties;

@Component
public class NiceAuth {
	
	final byte[] key;
	
	
    public NiceAuth(ApplicationProperties props) {
		super();
		this.key = (new Base32()).decode(props.getNice().getAuthKey());
	}

    public static final String M_GET_KEY = "getKey";
	public Integer getKey(){
		try {
            return getCode(key, (new Date().getTime() / 30000));
        } catch (NoSuchAlgorithmException | InvalidKeyException ex) {
            Logger.getLogger(NiceAuth.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            return null;
        }
    }
    
    
    private static int getCode(byte[] key, long t) throws NoSuchAlgorithmException, InvalidKeyException {
        byte[] data = new byte[8];
        long value = t;
        
        for (int i = 8; i-- > 0; value >>>= 8) {
          data[i] = (byte) value;
        }

        SecretKeySpec signKey = new SecretKeySpec(key, "HmacSHA1");
        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(signKey);
        byte[] hash = mac.doFinal(data);

        int offset = hash[20 - 1] & 0xF;

        long truncatedHash = 0;
        for (int i = 0; i < 4; ++i) {
          truncatedHash <<= 8;
          truncatedHash |= (hash[offset + i] & 0xFF);
        }

        truncatedHash &= 0x7FFFFFFF;
        truncatedHash %= 1000000;

        return (int) truncatedHash;
    }
}
