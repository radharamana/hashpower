package co.za.hash.domain.nice.global;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "stats" })
public class Result {

	@JsonProperty("stats")
	private List<Stat_> stats = null;

	@JsonProperty("stats")
	public List<Stat_> getStats() {
		return stats;
	}

	@JsonProperty("stats")
	public void setStats(List<Stat_> stats) {
		this.stats = stats;
	}

	

}