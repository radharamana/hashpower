package co.za.hash.domain.nice.global;

import java.math.BigDecimal;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "profitability_above_ltc", "price", "profitability_ltc", "algo", "speed", "profitability_btc",
		"profitability_above_btc", "profitability_eth", "profitability_above_eth" })
public class Stat_ {

	@JsonProperty("profitability_above_ltc")
	private String profitabilityAboveLtc;
	@JsonProperty("price")
	private BigDecimal price;
	@JsonProperty("profitability_ltc")
	private String profitabilityLtc;
	@JsonProperty("algo")
	private Integer algo;
	@JsonProperty("speed")
	private String speed;
	@JsonProperty("profitability_btc")
	private String profitabilityBtc;
	@JsonProperty("profitability_above_btc")
	private String profitabilityAboveBtc;
	@JsonProperty("profitability_eth")
	private String profitabilityEth;
	@JsonProperty("profitability_above_eth")
	private String profitabilityAboveEth;

	@JsonProperty("profitability_above_ltc")
	public String getProfitabilityAboveLtc() {
		return profitabilityAboveLtc;
	}

	@JsonProperty("profitability_above_ltc")
	public void setProfitabilityAboveLtc(String profitabilityAboveLtc) {
		this.profitabilityAboveLtc = profitabilityAboveLtc;
	}

	@JsonProperty("price")
	public BigDecimal getPrice() {
		return price;
	}

	@JsonProperty("price")
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@JsonProperty("profitability_ltc")
	public String getProfitabilityLtc() {
		return profitabilityLtc;
	}

	@JsonProperty("profitability_ltc")
	public void setProfitabilityLtc(String profitabilityLtc) {
		this.profitabilityLtc = profitabilityLtc;
	}

	@JsonProperty("algo")
	public Integer getAlgo() {
		return algo;
	}

	@JsonProperty("algo")
	public void setAlgo(Integer algo) {
		this.algo = algo;
	}

	@JsonProperty("speed")
	public String getSpeed() {
		return speed;
	}

	@JsonProperty("speed")
	public void setSpeed(String speed) {
		this.speed = speed;
	}

	@JsonProperty("profitability_btc")
	public String getProfitabilityBtc() {
		return profitabilityBtc;
	}

	@JsonProperty("profitability_btc")
	public void setProfitabilityBtc(String profitabilityBtc) {
		this.profitabilityBtc = profitabilityBtc;
	}

	@JsonProperty("profitability_above_btc")
	public String getProfitabilityAboveBtc() {
		return profitabilityAboveBtc;
	}

	@JsonProperty("profitability_above_btc")
	public void setProfitabilityAboveBtc(String profitabilityAboveBtc) {
		this.profitabilityAboveBtc = profitabilityAboveBtc;
	}

	@JsonProperty("profitability_eth")
	public String getProfitabilityEth() {
		return profitabilityEth;
	}

	@JsonProperty("profitability_eth")
	public void setProfitabilityEth(String profitabilityEth) {
		this.profitabilityEth = profitabilityEth;
	}

	@JsonProperty("profitability_above_eth")
	public String getProfitabilityAboveEth() {
		return profitabilityAboveEth;
	}

	@JsonProperty("profitability_above_eth")
	public void setProfitabilityAboveEth(String profitabilityAboveEth) {
		this.profitabilityAboveEth = profitabilityAboveEth;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(profitabilityAboveLtc).append(profitabilityLtc)
				.append(profitabilityAboveBtc).append(algo).append(price).append(speed).append(profitabilityAboveEth)
				.append(profitabilityEth).append(profitabilityBtc).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Stat_) == false) {
			return false;
		}
		Stat_ rhs = ((Stat_) other);
		return new EqualsBuilder().append(profitabilityAboveLtc, rhs.profitabilityAboveLtc)
				.append(profitabilityLtc, rhs.profitabilityLtc).append(profitabilityAboveBtc, rhs.profitabilityAboveBtc)
				.append(algo, rhs.algo).append(price, rhs.price).append(speed, rhs.speed)
				.append(profitabilityAboveEth, rhs.profitabilityAboveEth).append(profitabilityEth, rhs.profitabilityEth)
				.append(profitabilityBtc, rhs.profitabilityBtc).isEquals();
	}

}