package co.za.hash.domain.nice.myorder;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "orders","timestamp" })
public class Result {

	@JsonProperty("orders")
	private List<MyOrder> orders = null;
	private String timestamp;
	
	@JsonProperty("orders")
	public List<MyOrder> getOrders() {
		return orders;
	}

	@JsonProperty("orders")
	public void setOrders(List<MyOrder> orders) {
		this.orders = orders;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

}