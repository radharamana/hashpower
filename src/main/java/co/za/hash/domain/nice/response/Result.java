package co.za.hash.domain.nice.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "success","failure" })
public class Result {

	@JsonProperty("success")
	private String success;
	
	@JsonProperty("failure")
	private String failure;

	@JsonProperty("success")
	public String getSuccess() {
		return success;
	}

	@JsonProperty("success")
	public void setSuccess(String success) {
		this.success = success;
	}

	public String getFailure() {
		return failure;
	}

	public void setFailure(String failure) {
		this.failure = failure;
	}

}
