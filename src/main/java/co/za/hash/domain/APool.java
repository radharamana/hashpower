package co.za.hash.domain;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnore;

import co.za.hash.domain.nice.myorder.MyOrder;

public class APool{
	protected String name;
	private String host;
	private Integer port;
	private String user;
	private String pass;
	private Market prefMarket;
	private BigDecimal limitSpeed;
	
	public APool(){}
	
	
	public APool(MyOrder myOrder) {
		super();
		this.host = myOrder.getPoolHost();
		this.port = myOrder.getPoolPort();
		this.user = myOrder.getPoolUser();
		this.pass = myOrder.getPoolPass();
		Matcher matcher = Pattern.compile("(\\w*)(.com|.ca|.co)").matcher(this.host); matcher.find();
		this.name = matcher.group(1);
	}
	
	public APool(DPool prop, Market market){
		this.host = prop.getHost();
		this.port = prop.getPort();
		this.user = prop.getWorker();
		this.pass = prop.getPass();
		this.prefMarket = market;
		setName(prop.getName());
		setLimitSpeed(prop.getLimitSpeed());
	}
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}


	


	@Override
	public String toString() {
		return "Pool [name=" + name + ", host=" + host + ", port=" + port
				+ ", user=" + user + ", pass=" + pass + ", market=" + prefMarket.getLoc() + "]";
	}




	public BigDecimal getLimitSpeed() {
		return limitSpeed;
	}


	public void setLimitSpeed(BigDecimal limitSpeed) {
		this.limitSpeed = limitSpeed;
	}


	


	public Market getPrefMarket() {
		return prefMarket;
	}


	public void setPrefMarket(Market market) {
		this.prefMarket = market;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((host == null) ? 0 : host.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof APool))
			return false;
		APool other = (APool) obj;
		
		if (host == null) {
			if (other.host != null)
				return false;
		} else if (!host.equals(other.host))
			return false;
		return true;
	}


	@JsonIgnore
	public Integer getLoc() {
		return prefMarket.getLoc().ordinal();
	}


	@JsonIgnore
	public Integer getAlgo() {
		return prefMarket.getAlgo().getId();
	}
	
	
	
	
}
