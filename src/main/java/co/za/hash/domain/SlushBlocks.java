package co.za.hash.domain;

import java.util.Date;
import java.util.TreeMap;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"blocks", "active_workers", "round_started", "luck_30", "shares_cdf", "luck_b50", "luck_b10",
		"active_stratum", "ghashes_ps", "shares", "round_duration", "score", "luck_b250", "luck_7", "luck_1" })
public class SlushBlocks {
	@JsonProperty("blocks")
	private TreeMap<Long, SlushBlock> blocks;
	@JsonProperty("active_workers")
	private Integer activeWorkers;
	@JsonProperty("round_started")
	private String roundStarted;
	@JsonProperty("luck_30")
	private String luck30;
	@JsonProperty("shares_cdf")
	private String sharesCdf;
	@JsonProperty("luck_b50")
	private String luckB50;
	@JsonProperty("luck_b10")
	private String luckB10;
	@JsonProperty("active_stratum")
	private Integer activeStratum;
	@JsonProperty("ghashes_ps")
	private String ghashesPs;
	@JsonProperty("shares")
	private Long shares;
	@JsonFormat(pattern="HH:mm:ss")
	@JsonProperty("round_duration")
	private Date roundDuration;
	@JsonProperty("score")
	private String score;
	@JsonProperty("luck_b250")
	private String luckB250;
	@JsonProperty("luck_7")
	private String luck7;
	@JsonProperty("luck_1")
	private String luck1;

	@JsonProperty("active_workers")
	public Integer getActiveWorkers() {
		return activeWorkers;
	}

	@JsonProperty("active_workers")
	public void setActiveWorkers(Integer activeWorkers) {
		this.activeWorkers = activeWorkers;
	}

	@JsonProperty("round_started")
	public String getRoundStarted() {
		return roundStarted;
	}

	@JsonProperty("round_started")
	public void setRoundStarted(String roundStarted) {
		this.roundStarted = roundStarted;
	}

	@JsonProperty("luck_30")
	public String getLuck30() {
		return luck30;
	}

	@JsonProperty("luck_30")
	public void setLuck30(String luck30) {
		this.luck30 = luck30;
	}

	@JsonProperty("shares_cdf")
	public String getSharesCdf() {
		return sharesCdf;
	}

	@JsonProperty("shares_cdf")
	public void setSharesCdf(String sharesCdf) {
		this.sharesCdf = sharesCdf;
	}

	@JsonProperty("luck_b50")
	public String getLuckB50() {
		return luckB50;
	}

	@JsonProperty("luck_b50")
	public void setLuckB50(String luckB50) {
		this.luckB50 = luckB50;
	}

	@JsonProperty("luck_b10")
	public String getLuckB10() {
		return luckB10;
	}

	@JsonProperty("luck_b10")
	public void setLuckB10(String luckB10) {
		this.luckB10 = luckB10;
	}

	@JsonProperty("active_stratum")
	public Integer getActiveStratum() {
		return activeStratum;
	}

	@JsonProperty("active_stratum")
	public void setActiveStratum(Integer activeStratum) {
		this.activeStratum = activeStratum;
	}

	@JsonProperty("ghashes_ps")
	public String getGhashesPs() {
		return ghashesPs;
	}

	@JsonProperty("ghashes_ps")
	public void setGhashesPs(String ghashesPs) {
		this.ghashesPs = ghashesPs;
	}

	@JsonProperty("shares")
	public Long getShares() {
		return shares;
	}

	@JsonProperty("shares")
	public void setShares(Long shares) {
		this.shares = shares;
	}

	@JsonProperty("round_duration")
	public Date getRoundDuration() {
		return roundDuration;
	}

	@JsonProperty("round_duration")
	public void setRoundDuration(Date roundDuration) {
		this.roundDuration = roundDuration;
	}

	@JsonProperty("score")
	public String getScore() {
		return score;
	}

	@JsonProperty("score")
	public void setScore(String score) {
		this.score = score;
	}

	@JsonProperty("luck_b250")
	public String getLuckB250() {
		return luckB250;
	}

	@JsonProperty("luck_b250")
	public void setLuckB250(String luckB250) {
		this.luckB250 = luckB250;
	}

	@JsonProperty("luck_7")
	public String getLuck7() {
		return luck7;
	}

	@JsonProperty("luck_7")
	public void setLuck7(String luck7) {
		this.luck7 = luck7;
	}

	@JsonProperty("luck_1")
	public String getLuck1() {
		return luck1;
	}

	@JsonProperty("luck_1")
	public void setLuck1(String luck1) {
		this.luck1 = luck1;
	}
	
	@JsonProperty("blocks")
	public TreeMap<Long, SlushBlock> getBlocks() {
		return blocks;
	}

	@JsonProperty("blocks")
	public void setBlocks(TreeMap<Long, SlushBlock> blocks) {
		this.blocks = blocks;
	}

}
