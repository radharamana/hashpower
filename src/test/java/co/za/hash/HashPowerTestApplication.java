package co.za.hash;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import co.za.hash.config.ApplicationProperties;


@SpringBootApplication(scanBasePackageClasses=ApplicationProperties.class)
public class HashPowerTestApplication {
	public static void main(String[] args) {
		SpringApplication.run(HashPowerTestApplication.class, args);
	}
}
