package co.za.hash;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogHeaderProcessor implements Processor {
	Logger log = LoggerFactory.getLogger(getClass());
	
	final String name;
	
	public LogHeaderProcessor(String name) {
		super();
		this.name = name;
	}

	@Override
	public void process(Exchange exchange) throws Exception {
		log.info((String)exchange.getIn().getHeader(name));

	}

}
