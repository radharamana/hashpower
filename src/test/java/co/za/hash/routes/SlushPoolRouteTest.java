package co.za.hash.routes;

import static co.za.hash.util.LamdaUtil.entriesToMap;
import static co.za.hash.util.LamdaUtil.entry;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.apache.camel.builder.RouteBuilder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;

import co.za.hash.HashTestSupport;
import co.za.hash.domain.SlushBlocks;
import co.za.hash.routes.nice.NiceRoute;

public class SlushPoolRouteTest extends HashTestSupport {
	


	private static final String SHA256 = "sha256";


	public SlushPoolRouteTest() throws JsonParseException, JsonMappingException, IOException {
		//super(SlushPoolRoute.ROUTE_ID, Arrays.asList(NiceRoute.ROUTE_NEW_ORDER,NiceRoute.ROUTE_SPEED,NiceRoute.ROUTE_DEC_PRICE, NiceRoute.ID_NEW_ORDER_REST));
		
		super(SlushPoolRoute.ROUTE_ID, Collections.unmodifiableMap(Stream.of( 
				entry(SlushPoolRoute.ROUTE_ID, Arrays.asList(SlushPoolRoute.ID_REST)),//Arrays.asList(NiceRoute.ROUTE_NEW_ORDER,NiceRoute.ROUTE_SPEED,NiceRoute.ROUTE_DEC_PRICE)),
				entry(NiceRoute.ROUTE_NEW_ORDER, Arrays.asList(NiceRoute.ID_NEW_ORDER_REST)),
				entry(NiceRoute.ROUTE_PRICE_INC, Arrays.asList(NiceRoute.ID_INC_PRICE_REST)),
				entry(NiceRoute.ROUTE_SPEED, Arrays.asList(NiceRoute.ID_SPEED_REST)),
				entry(NiceRoute.ROUTE_PRICE_DEC, Arrays.asList(NiceRoute.ID_DEC_PRICE_REST))
				).collect(entriesToMap())));
		
		slushBlocks = mapper.readValue(this.getClass().getResource("slush.json"), new TypeReference<SlushBlocks>() {});
	}
	
	@Override
	protected List<RouteBuilder> initRoutes() {
		return Arrays.asList(new SlushPoolRoute(props, orderService, poolService)
				,new NiceRoute(props, walletService, orderService, niceAuth, poolService));
	}

	SlushBlocks slushBlocks;
//	
//	
//	@Test
//	public void testNewOrderRoute() throws CamelExecutionException, IOException, InterruptedException {
//		MockEndpoint mockRest = mocks.get(NiceRoute.ID_NEW_ORDER_REST);
//		mockRest.expectedMessageCount(1);
//		mockRest.returnReplyBody(ExpressionBuilder.constantExpression(getFileString("nice_success_new_order.json")));
//		
//		
//		template.sendBody(direct, mapper.writeValueAsString(slushBlocks));
//		
//		Order order = orderService.getOrderForPool("slush");
//		assertEquals(new Integer(5569), order.getId());
//		String restUrl = (String) mockRest.getReceivedExchanges().get(0).getIn().getHeader("rest");
//		String expectedUrl = "https://api.nicehash.com/api?method=orders.create&id=51467&key=e5af66da-2e0d-d9a1-736d-163c698a6bb1"
//				+ "&location=0&algo=1&amount=0.05&price=0.0625&limit=3&pool_host=eu.stratum.slushpool.com&pool_port=3333&pool_user=radharamana.worker1&pool_pass=x";
//		String rest = getRestHeader(mockRest);
//		log.info(rest);
//		assertEquals(getNewOrderRest(order), rest );
//		assertEquals(new BigDecimal("1").subtract(order.getBtcAvailable()), walletService.getBtcAvailable());
//		
//		assertMockEndpointsSatisfied();
//	}
//
//	@Test
//	public void testNewOrderWalletEmptyRoute() throws CamelExecutionException, JsonProcessingException, InterruptedException{
//		MockEndpoint mockRest = mocks.get(NiceRoute.ID_NEW_ORDER_REST);
//		mockRest.expectedMessageCount(0);
//		
//		walletService.setBtcAvailable(new BigDecimal("0"));
//		
//		template.sendBody(direct, mapper.writeValueAsString(slushBlocks));
//		
//		assertMockEndpointsSatisfied();
//	}
//	
//	@Test
//	public void testBlockActiveWithOrder() throws CamelExecutionException, JsonProcessingException, InterruptedException{
//		MockEndpoint mockRest = mocks.get(NiceRoute.ID_NEW_ORDER_REST);
//		mockRest.expectedMessageCount(0);
//		
//		orderService.addOrder(1, orderService.doCreateOrder(props.getPool().getSlush().getName()));
//		
//		template.sendBody(direct, mapper.writeValueAsString(slushBlocks));
//		
//		assertMockEndpointsSatisfied();
//	}
//	
//	@Test
//	public void testBlockActiveWithDormantOrder() throws CamelExecutionException, JsonProcessingException, InterruptedException{
//		mocks.get(NiceRoute.ID_NEW_ORDER_REST).expectedMessageCount(0);
//		
//		MockEndpoint speedRest = doSuccessRest(NiceRoute.ID_SPEED_REST, successStd);
//		MockEndpoint incPriceRest = doSuccessRest(NiceRoute.ID_INC_PRICE_REST, successStd);
//		
//		assertEquals(0, orderService.getOrders().size());
//		
//		
//		Order dorOrder = orderService.doCreateOrder(props.getPool().getSlush().getName());
//		dorOrder.setLimitSpeed(props.getNice().getAlgoMap().get(SHA256).getMinSpeed());
//		dorOrder.setPrice(new BigDecimal("0.05"));
//		orderService.addOrder(1, dorOrder);
//		
//		template.sendBody(direct, mapper.writeValueAsString(slushBlocks));
//		
//		String speedRestExp = "https://api.nicehash.com/api?method=orders.set.limit&id=51467&key=e5af66da-2e0d-d9a1-736d-163c698a6bb1"
//				+ "&location=0&algo=1&order=1&limit="+props.getPool().getSlush().getLimitSpeed();
//		String incPriceRestExp = "https://api.nicehash.com/api?method=orders.set.price&id=51467&key=e5af66da-2e0d-d9a1-736d-163c698a6bb1"
//				+ "&location=0&algo=1&order=1&price=0.0625";
//		
//		assertEquals(dorOrder.getLimitSpeed(),props.getPool().getSlush().getLimitSpeed());
//			
//		assertEquals(getSpeedLimitRest(1),getRestHeader(speedRest));
//		assertEquals(getPriceIncRest(1, dorOrder.getPool().getMarket()), getRestHeader(incPriceRest));
//		
//		
//		assertMockEndpointsSatisfied();
//	}
//	
//	@Test
//	public void testBlockNotActiveWithOrder() throws CamelExecutionException, JsonProcessingException, InterruptedException{
//		
//		
//		MockEndpoint speedRest = doSuccessRest(NiceRoute.ID_SPEED_REST, successStd);
//		MockEndpoint decPriceRest = doSuccessRest(NiceRoute.ID_DEC_PRICE_REST, successStd);
//		
//		Order activeOrder = orderService.doCreateOrder(props.getPool().getSlush().getName());
//		orderService.addOrder(1, activeOrder);
//		slushBlocks.setRoundDuration(new Date(props.getPool().getSlush().getBlockMins()*61*1000));
//		
//		template.sendBody(direct, mapper.writeValueAsString(slushBlocks));
//		
//		assertEquals(activeOrder.getPool().getMarket().getMinPrice().subtract(props.getNice().getDecAmount()), activeOrder.getPrice());
//		assertEquals(props.getNice().getAlgoMap().get(SHA256).getMinSpeed(), activeOrder.getLimitSpeed());
//		
//		String expSpeedRest = "https://api.nicehash.com/api?method=orders.set.limit&id=51467&key=e5af66da-2e0d-d9a1-736d-163c698a6bb1"
//				+ "&location=0&algo=1&order=1&limit=0.05";
//		String expDecPriceRest = "https://api.nicehash.com/api?method=orders.set.price.decrease&id=51467&key=e5af66da-2e0d-d9a1-736d-163c698a6bb1"
//				+ "&location=0&algo=1&order=1";
//		assertEquals(expSpeedRest, (String)speedRest.getReceivedExchanges().get(0).getIn().getHeader("rest"));
//		assertEquals(expDecPriceRest,(String)decPriceRest.getReceivedExchanges().get(0).getIn().getHeader("rest"));
//		
//		
//		assertMockEndpointsSatisfied();
//	}

	
	


}
