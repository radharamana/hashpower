package co.za.hash.routes;

import static co.za.hash.util.LamdaUtil.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.apache.camel.CamelExecutionException;
import org.apache.camel.builder.ExpressionBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;

import co.za.hash.HashTestSupport;
import co.za.hash.Weave;
import co.za.hash.domain.APool;
import co.za.hash.domain.Order;
import co.za.hash.domain.nice.market.NiceHashMarket;
import co.za.hash.routes.nice.MarketRoute;
import co.za.hash.routes.nice.NiceRoute;

public class MarketRouteTest extends HashTestSupport {

	String market;
	NiceHashMarket niceHashMarket;
	
	public MarketRouteTest() throws JsonProcessingException, IOException {
		//super(new MarketRoute(), MarketRoute.ROUTE_ID, "rest,inc,speed,dec1,dec2,dec3");
		super(Collections.unmodifiableMap(Stream.of( 
				entry(MarketRoute.ROUTE_ID, Arrays.asList(new Weave(MarketRoute.ID_REST), new Weave(MarketRoute.ID_DEC1,false))),//Arrays.asList(NiceRoute.ROUTE_NEW_ORDER,NiceRoute.ROUTE_SPEED,NiceRoute.ROUTE_DEC_PRICE)),
				entry(NiceRoute.ROUTE_PRICE_INC, Arrays.asList(new Weave(NiceRoute.ID_INC_PRICE_REST))),
				entry(NiceRoute.ROUTE_SPEED, Arrays.asList(new Weave(NiceRoute.ID_SPEED_REST))),
				entry(NiceRoute.ROUTE_PRICE_DEC, Arrays.asList(new Weave(NiceRoute.ID_DEC_PRICE_REST))),
				entry(NiceRoute.ROUTE_REFILL, Arrays.asList(new Weave(NiceRoute.ID_REFILL_REST)))
				).collect(entriesToMap())), MarketRoute.ROUTE_ID);
		
		
		
		market = getFileString("nicehash_market.json");
		niceHashMarket = mapper.readValue(market, NiceHashMarket.class);
		
	}
	
	@Before
	@Override
	public void setup() throws Exception{
		super.setup();
		String myOrders = getFileString("myOrders.json");
		orderService.setOrders(mapper.readValue(myOrders, new TypeReference<List<Order>>() {}));
	}
	
//	@Test
//	public void testDecreaseSpeed() throws InterruptedException{
//		MockEndpoint speedRest = doSuccessRest(NiceRoute.ID_SPEED_REST, successStd);
//		MockEndpoint decRest = doSuccessRest(NiceRoute.ID_DEC_PRICE_REST, successStd);
//		mocks.get(MarketRoute.ID_DEC2).expectedMessageCount(1);
//		
//		Integer id = new Integer(1426319);
//		
//		APool pool = orderService.getOrders().get(id).getPool();
//		template.sendBody(direct,market);
//		
//		assertEquals(new BigDecimal("0.0693"), pool.getMarket().getMinPrice());
//		String rest = getRestHeader(speedRest);
//		log.info(rest);
//		assertEquals(getSpeedMinRest(id, props.getNice().getAlgoMap().get("sha256")), rest);
//		rest = getRestHeader(decRest);
//		log.info(rest);
//		assertEquals(getPriceDecRest(id), rest);
//		
//		assertMockEndpointsSatisfied();
//	}
	
	@Test
	public void testIncreasePriceFailure() throws InterruptedException, JsonProcessingException{
		MockEndpoint priceIncRest = doResponseRest(NiceRoute.ID_INC_PRICE_REST, failureStd);
		Order order = orderService.getOrders().get(1879);
		order.getMarket().setMaxPrice(new BigDecimal("0.07"));
		order = orderService.getOrders().get(1426319);
		order.getMarket().setMaxPrice(new BigDecimal("0.07"));
		Order myOrder = niceHashMarket.getResult().getOrders().stream().filter(o->o.getId() == 1426319).findFirst().get();
		myOrder.setWorkers(0);
		
		
		mocks.get(MarketRoute.ID_REST).returnReplyBody(ExpressionBuilder.constantExpression(mapper.writeValueAsString(niceHashMarket)));
		template.sendBody(direct,"");
		
		String rest = getRestHeader(priceIncRest);
		log.info(rest);
		assertEquals(getPriceIncRest(1426319, order.getMarket()), rest);
		
		assertMockEndpointsSatisfied();
	}

	@Test
	public void testIncreasePrice() throws InterruptedException, JsonProcessingException{
		MockEndpoint priceIncRest = doResponseRest(NiceRoute.ID_INC_PRICE_REST, successStd);
		Order order = orderService.getOrders().get(1879);
		order.getMarket().setMaxPrice(new BigDecimal("0.07"));
		order = orderService.getOrders().get(1426319);
		order.getMarket().setMaxPrice(new BigDecimal("0.07"));
		Order myOrder = niceHashMarket.getResult().getOrders().stream().filter(o->o.getId() == 1426319).findFirst().get();
		myOrder.setWorkers(0);
		
		
		mocks.get(MarketRoute.ID_REST).returnReplyBody(ExpressionBuilder.constantExpression(mapper.writeValueAsString(niceHashMarket)));
		template.sendBody(direct,"");
		
		String rest = getRestHeader(priceIncRest);
		log.info(rest);
		assertEquals(getPriceIncRest(1426319, order.getMarket()), rest);
		
		assertMockEndpointsSatisfied();
	}
	
	@Test
	public void testIncreasePrice2() throws InterruptedException, JsonProcessingException{
		MockEndpoint priceIncRest = doResponseRest(NiceRoute.ID_INC_PRICE_REST, successStd);
		
		Order order = orderService.getOrders().get(1426319);
		APool pool = order.getPool();
		pool.getPrefMarket().setMaxPrice(new BigDecimal("0.065"));
		//pool.getMarket().setMinPrice(new BigDecimal("0.071"));
		Order orderInMarket = niceHashMarket.getResult().getOrders().stream().filter(o->o.getId() == 1426319).findFirst().get();
		orderInMarket.setWorkers(0);
		
		orderInMarket.setPrice(new BigDecimal("0.06"));
		
		mocks.get(MarketRoute.ID_REST).returnReplyBody(ExpressionBuilder.constantExpression(mapper.writeValueAsString(niceHashMarket)));
		mocks.get(NiceRoute.ID_INC_PRICE_REST).expectedMessageCount(0);
		
		template.sendBody(direct,"");
		
		
		
		assertMockEndpointsSatisfied();
	}
	
	@Test
	public void testIncreasePriceRefill() throws InterruptedException, JsonProcessingException{
		MockEndpoint priceIncRest = doResponseRest(NiceRoute.ID_INC_PRICE_REST, successStd);
		MockEndpoint refillRest = doResponseRest(NiceRoute.ID_REFILL_REST, getFileStream("nice_success_response.json"));
		Order order = orderService.getOrders().get(1879);
		order.getMarket().setMaxPrice(new BigDecimal("0.07"));
		order = orderService.getOrders().get(1426319);
		order.getMarket().setMaxPrice(new BigDecimal("0.07"));
		Order orderInMarket = niceHashMarket.getResult().getOrders().stream().filter(o->o.getId() == 1426319).findFirst().get();
		orderInMarket.setWorkers(0);
		order.setBtcAvailable(new BigDecimal("0.0001"));
		
		mocks.get(MarketRoute.ID_REST).returnReplyBody(ExpressionBuilder.constantExpression(mapper.writeValueAsString(niceHashMarket)));
		template.sendBody(direct,"");
		
		String rest = getRestHeader(priceIncRest);
		log.info(rest);
		assertEquals(getPriceIncRest(1426319, order.getMarket()), rest);
		
		rest = getRestHeader(refillRest);
		log.info(rest);
		assertEquals(getRefillRest(1426319, props.getNice().getOrder().getRefill()), rest);
		
		assertMockEndpointsSatisfied();
	}
	
	@Test
	public void testDecPrice1() throws InterruptedException, CamelExecutionException, JsonProcessingException{
		MockEndpoint decRest = doResponseRest(NiceRoute.ID_DEC_PRICE_REST, successStd);
		mocks.get(MarketRoute.ID_DEC1).expectedMessageCount(1);
		
		//getMockEndpoint("mock:dec1").expectedMessageCount(1);
		Order myOrder = niceHashMarket.getResult().getOrders().stream().filter(o->o.getId() == 1426319).findFirst().get();
		myOrder.setAcceptedSpeed(myOrder.getLimitSpeed().floatValue() *10000000);
		myOrder.setPrice(new BigDecimal("0.0693").multiply(new BigDecimal("1.04")));

		mocks.get(MarketRoute.ID_REST).returnReplyBody(ExpressionBuilder.constantExpression(mapper.writeValueAsString(niceHashMarket)));
		template.sendBody(direct,"");
		
		String rest = getRestHeader(decRest);
		log.info(rest);
		assertEquals(getPriceDecRest(1426319), rest);
		
		assertMockEndpointsSatisfied();
	}
	
	
	@Test
	public void testDecFailedSpeedDec() throws JsonProcessingException, InterruptedException{
		MockEndpoint decRest = doResponseRest(NiceRoute.ID_DEC_PRICE_REST, failureStdStr);
		MockEndpoint decSpeedRest = doResponseRest(NiceRoute.ID_SPEED_REST, successStdStr);
		
		//mocks.get(MarketRoute.ID_DEC1).expectedMessageCount(1);
		
		//getMockEndpoint("mock:dec1").expectedMessageCount(1);
		Order myOrder = niceHashMarket.getResult().getOrders().stream().filter(o->o.getId() == 1426319).findFirst().get();
		myOrder.setAcceptedSpeed(myOrder.getLimitSpeed().floatValue() *10000000);
		myOrder.setPrice(new BigDecimal("0.0693").multiply(new BigDecimal("1.04")));

		mocks.get(MarketRoute.ID_REST).returnReplyBody(ExpressionBuilder.constantExpression(mapper.writeValueAsString(niceHashMarket)));
		template.sendBody(direct,"");
		
		String rest = getRestHeader(decRest);
		log.info(rest);
		assertEquals(getPriceDecRest(1426319), rest);
		
		rest = getRestHeader(decSpeedRest);
		log.info(rest);
		assertEquals(getSpeedMinRest(1426319, props.getNice().getAlgoMap().get("sha256")), rest);
		
		assertMockEndpointsSatisfied();
	}
	
	@Test
	public void testDecFailedSpeedNotDec() throws JsonProcessingException, InterruptedException{
		testDecFailedSpeedDec();
		mocks.get(NiceRoute.ID_DEC_PRICE_REST).expectedMessageCount(2);
		Order myOrder = niceHashMarket.getResult().getOrders().stream().filter(o->o.getId() == 1426319).findFirst().get();
		myOrder.setLimitSpeed(BigDecimal.ZERO);
		mocks.get(MarketRoute.ID_REST).returnReplyBody(ExpressionBuilder.constantExpression(mapper.writeValueAsString(niceHashMarket)));
		template.sendBody(direct,"");
		assertMockEndpointsSatisfied();
	}
	
	@Test
	public void testIncIncSpeed() throws JsonProcessingException, InterruptedException{
		MockEndpoint priceIncRest = doResponseRest(NiceRoute.ID_INC_PRICE_REST, getFileStream("nice_success_response.json"));
		MockEndpoint speedIncRest = doResponseRest(NiceRoute.ID_SPEED_REST, getFileStream("nice_success_response.json"));
		
		Order order = orderService.getOrders().get(1879);
		order.getMarket().setMaxPrice(new BigDecimal("0.07"));
		order = orderService.getOrders().get(1426319);
		order.getMarket().setMaxPrice(new BigDecimal("0.07"));
		Order myOrder = niceHashMarket.getResult().getOrders().stream().filter(o->o.getId() == 1426319).findFirst().get();
		myOrder.setWorkers(0);
		myOrder.setLimitSpeed(BigDecimal.ZERO);
		
		
		mocks.get(MarketRoute.ID_REST).returnReplyBody(ExpressionBuilder.constantExpression(mapper.writeValueAsString(niceHashMarket)));
		template.sendBody(direct,"");
		
		String rest = getRestHeader(priceIncRest);
		log.info(rest);
		assertEquals(getPriceIncRest(1426319, order.getMarket()), rest);
		
		rest = getRestHeader(speedIncRest);
		log.info(rest);
		assertEquals(getSpeedLimitRest(1426319), rest);
		
		assertMockEndpointsSatisfied();
		
	}

	
//	@Test
//	public void testDecPrice3() throws InterruptedException, CamelExecutionException, JsonProcessingException{
//		MockEndpoint decRest = doSuccessRest(NiceRoute.ID_DEC_PRICE_REST, successStd);
//		mocks.get(MarketRoute.ID_DEC3).expectedMessageCount(1);
//		//getMockEndpoint("mock:dec3").expectedMessageCount(1);
//		Order myOrder = niceHashMarket.getResult().getOrders().stream().filter(o->o.getId() == 1426319).findFirst().get();
//		
//		myOrder.setLimitSpeed(new BigDecimal("0.05"));
//		
//		template.sendBody(direct,mapper.writeValueAsString(niceHashMarket));
//		
//		String rest = getRestHeader(decRest);
//		log.info(rest);
//		assertEquals(getPriceDecRest(1426319), rest);
//		
//		assertMockEndpointsSatisfied();
//	}

	@Override
	protected List<RouteBuilder> initRoutes() {
		return Arrays.asList(new MarketRoute(props, orderService, marketService, multiPoolService)
				,new NiceRoute(props, walletService, orderService, niceAuth, poolService));
	}
	
	

}
