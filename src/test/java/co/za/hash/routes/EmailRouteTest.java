package co.za.hash.routes;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.camel.builder.RouteBuilder;
import org.junit.Test;

import co.za.hash.HashTestSupport;

public class EmailRouteTest extends HashTestSupport{
	public EmailRouteTest(){
		super(EmailRoute.ROUTE_BASIC, new HashMap<String, List<String>>());
	}

	@Override
	protected List<RouteBuilder> initRoutes() {
		return Arrays.asList(new EmailRoute(props));
	}
	
	//@Test
	public void testEmail(){
		template.sendBody(direct,"Hello World");
	}
}
