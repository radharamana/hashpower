package co.za.hash.routes;

import static co.za.hash.util.LamdaUtil.entriesToMap;
import static co.za.hash.util.LamdaUtil.entry;
import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;

import co.za.hash.HashTestSupport;
import co.za.hash.domain.Location;
import co.za.hash.domain.Market;
import co.za.hash.domain.Order;
import co.za.hash.domain.nice.myorder.MyOrder;
import co.za.hash.domain.nice.myorder.MyOrderAll;
import co.za.hash.routes.nice.MyOrdersRoute;
import co.za.hash.routes.nice.NiceRoute;



public class MyOrdersRouteTest extends HashTestSupport{
	
	
	String myOrdersNice, emptyOrdersJson, myOrdersLocal;

	public MyOrdersRouteTest() throws JsonParseException, JsonMappingException, IOException {
		//super(new MyOrdersRoute(props, marketService), "myOrders", "rest,refill");
		//super(MyOrdersRoute.ROUTE_ID, Arrays.asList(MyOrdersRoute.ID_REST,NiceRoute.ROUTE_REFILL));
		super(MyOrdersRoute.ROUTE_FIND_NEW, Collections.unmodifiableMap(Stream.of( 
				entry(MyOrdersRoute.ROUTE_FIND_NEW, Arrays.asList(MyOrdersRoute.ID_REST_FIND_NEW))//Arrays.asList(NiceRoute.ROUTE_NEW_ORDER,NiceRoute.ROUTE_SPEED,NiceRoute.ROUTE_DEC_PRICE)),
				).collect(entriesToMap())));
		
		myOrdersNice = getFileString("nice_my_orders.json");
		emptyOrdersJson = getFileString("nice_my_orders_empty.json");
		myOrdersLocal = getFileString("myOrders.json");
	}
	
	@Override
	protected List<RouteBuilder> initRoutes() {
		return Arrays.asList(new MyOrdersRoute(props, poolService, orderService, marketService)
				,new NiceRoute(props, walletService, orderService, niceAuth, poolService));
	}

	
	@Before
	public void setMock(){
		MockEndpoint mockMyOrder = mocks.get(MyOrdersRoute.ID_REST_FIND_NEW);
		mockMyOrder.whenAnyExchangeReceived(new Processor(){
			@Override
			public void process(Exchange exchange) throws Exception {
				Market m = ((Market)exchange.getIn().getHeader("market"));
				if(m.getAlgo().getId() == 1 && m.getLoc() == Location.EU){
					exchange.getIn().setBody(myOrdersNice);
				}else{
					exchange.getIn().setBody(emptyOrdersJson);
				}
				
			}
		});
	}
	
		
	@Test
	public void testMyOrders() throws InterruptedException, JsonGenerationException, JsonMappingException, IOException{
		template.sendBody(direct,"");
		
		//mapper.writeValue(new File("myOrders.json"), orderService.getOrders().values());
		
		List<Order> ordExp = mapper.readValue(myOrdersLocal, new TypeReference<List<Order>>(){});
		log.info("orders:{}", mapper.writeValueAsString(orderService.getOrders().values()));
		
		assertThat(orderService.getOrders().values(), sameBeanAs(ordExp));
		//System.out.println();
		
	}
	
	@Test
	public void testMyOrdersNoId() throws JsonParseException, JsonMappingException, IOException{
		orderService.setOrders(mapper.readValue(myOrdersLocal, new TypeReference<List<Order>>() {}));
		
		Order noId = orderService.getOrders().get(1426319);
		orderService.getOrders().remove(1426319);
		noId.setId(-1426319);
		orderService.getOrders().put(-1426319, noId);
		
		template.sendBody(direct,"");
		
		assertNotNull(orderService.getOrders().get(1426319));
		
	}
	
	
	//@Test
	public void testRefill() throws JsonParseException, JsonMappingException, IOException, InterruptedException{
		mocks.get(MyOrdersRoute.ID_REST_FIND_NEW).whenExchangeReceived(2, new EmptyOrderProcessor());
		
		MyOrderAll myOrderAll = mapper.readValue(myOrdersNice, MyOrderAll.class);
		MyOrder myOrder = myOrderAll.getResult().getOrders().get(0);
		myOrder.setBtcAvail(new BigDecimal("0.009"));
		MockEndpoint refillRest = doResponseRest(NiceRoute.ID_REFILL_REST, successStd);
		
		template.sendBody(direct,mapper.writeValueAsString(myOrderAll));
		
		String rest = getRestHeader(refillRest);
		log.info(rest);
		assertEquals(getRefillRest(myOrder.getId(), props.getNice().getOrder().getRefill()), rest);
		
		assertMockEndpointsSatisfied();
	}
	
	//@Test
	public void testExpiredFailed() throws JsonParseException, JsonMappingException, IOException, InterruptedException{
		
		mocks.get(MyOrdersRoute.ID_REST_FIND_NEW).whenExchangeReceived(2, new EmptyOrderProcessor());
		template.sendBody(direct,myOrdersNice);
		
		MyOrderAll myOrderAll = mapper.readValue(myOrdersNice, MyOrderAll.class);
		myOrderAll.getResult().getOrders().remove(1);
		mocks.get(MyOrdersRoute.ID_REST_FIND_NEW).whenExchangeReceived(4, new EmptyOrderProcessor());
		mocks.get(NiceRoute.ID_NEW_ORDER_REST).expectedMessageCount(0);
		mocks.get(NiceRoute.ID_ORDER_FOR_POOL_ALREADY).expectedMessageCount(1);
		template.sendBody(direct,mapper.writeValueAsString(myOrderAll));
		
		assertMockEndpointsSatisfied();
	}
	
	//@Test
	public void testExpired() throws Exception {
		mocks.get(MyOrdersRoute.ID_REST_FIND_NEW).whenExchangeReceived(2, new EmptyOrderProcessor());
		template.sendBody(direct,myOrdersNice);
		
		MyOrderAll myOrderAll = mapper.readValue(myOrdersNice, MyOrderAll.class);
		myOrderAll.getResult().getOrders().remove(1);
		myOrderAll.getResult().getOrders().remove(0);
		mocks.get(MyOrdersRoute.ID_REST_FIND_NEW).whenExchangeReceived(4, new EmptyOrderProcessor());
		mocks.get(NiceRoute.ID_NEW_ORDER_REST).expectedMessageCount(1);
		mocks.get(NiceRoute.ID_ORDER_FOR_POOL_ALREADY).expectedMessageCount(1);
		doResponseRest(NiceRoute.ID_NEW_ORDER_REST, successNewOrder);
		
		template.sendBody(direct,mapper.writeValueAsString(myOrderAll));
		
		assertMockEndpointsSatisfied();
	}
}
