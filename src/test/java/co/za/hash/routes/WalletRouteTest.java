package co.za.hash.routes;

import static co.za.hash.util.LamdaUtil.entriesToMap;
import static co.za.hash.util.LamdaUtil.entry;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.apache.camel.builder.ExpressionBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.junit.Test;

import co.za.hash.HashTestSupport;
import co.za.hash.Weave;
import co.za.hash.routes.nice.WalletRoute;

public class WalletRouteTest extends HashTestSupport {
	String wallet;
	public WalletRouteTest(){
		super(Collections.unmodifiableMap(Stream.of( 
				entry(WalletRoute.ROUTE_ID, Arrays.asList(new Weave(WalletRoute.ID_REST)))
				).collect(entriesToMap())), WalletRoute.ROUTE_ID);	
		wallet = getFileString("wallet.json");
	}
	@Override
	protected List<RouteBuilder> initRoutes() {
		return Arrays.asList(new WalletRoute(props, walletService));
	}
	
	@Test
	public void testWallet(){
		mocks.get(WalletRoute.ID_REST).returnReplyBody(ExpressionBuilder.constantExpression(wallet));
		template.sendBody(direct,wallet);
		assertEquals(new BigDecimal("0.00500000"), walletService.getBtcAvailable());
		
	}
}
