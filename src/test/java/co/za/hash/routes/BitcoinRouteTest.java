package co.za.hash.routes;

import static co.za.hash.util.LamdaUtil.entriesToMap;
import static co.za.hash.util.LamdaUtil.entry;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.apache.camel.builder.ExpressionBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import co.za.hash.HashTestSupport;
import co.za.hash.Weave;
import co.za.hash.domain.Order;
import co.za.hash.domain.nice.market.NiceHashMarket;
import co.za.hash.routes.nice.MarketRoute;
import co.za.hash.routes.nice.NiceRoute;

public class BitcoinRouteTest extends HashTestSupport {
	String market;
	InputStream wtm;
	NiceHashMarket niceHashMarket;
	
	public BitcoinRouteTest() throws JsonParseException, JsonMappingException, IOException{
		super(Collections.unmodifiableMap(Stream.of( 
				entry(BitcoinRoute.ROUTE_ID, Arrays.asList(new Weave(BitcoinRoute.ID_NICE_BTC_MARKET), new Weave(BitcoinRoute.ID_WTM_REST))),
				entry(NiceRoute.ROUTE_NEW_ORDER, Arrays.asList(new Weave(NiceRoute.ID_NEW_ORDER_REST), new Weave(NiceRoute.ID_ORDER_FOR_POOL_ALREADY))),
				entry(EmailRoute.ROUTE_NEW_ORDER, Arrays.asList(new Weave(EmailRoute.ID_BITCOIN_EMAIL)))
				).collect(entriesToMap())), BitcoinRoute.ROUTE_ID);
		market = getFileString("nicehash_market.json");
		niceHashMarket = mapper.readValue(market, NiceHashMarket.class);
		wtm = getFileStream("wtm.json");
	}

	@Override
	protected List<RouteBuilder> initRoutes() {
		return Arrays.asList(new BitcoinRoute(props, marketService, poolService)
				,new NiceRoute(props, walletService, orderService, niceAuth, poolService)
				,new EmailRoute(props));
	}
	
	@Test
	public void testOpenNewOrder() throws InterruptedException{
		mocks.get(BitcoinRoute.ID_NICE_BTC_MARKET).returnReplyBody(ExpressionBuilder.constantExpression(market));
		MockEndpoint newOrderRest = doResponseRest(NiceRoute.ID_NEW_ORDER_REST, successNewOrderBlank);
		
		template.sendBody(direct,getFileStream("wtm.json"));
		
		String rest = getRestHeader(newOrderRest);
		Order order = orderService.getOrderForPool("bitcoin");
		System.out.println("Rest:"+rest);
		assertEquals(getNewOrderRest(order), rest );
		
		assertMockEndpointsSatisfied();
	}
	
	@Test
	public void testOldOrderNewOrderNotCreated() throws InterruptedException{
		testOpenNewOrder();
		
		mocks.get(NiceRoute.ID_ORDER_FOR_POOL_ALREADY).expectedMessageCount(1);
		template.sendBody(direct,getFileStream("wtm.json"));
		
		assertMockEndpointsSatisfied();
		
	}
	
	
	

}
