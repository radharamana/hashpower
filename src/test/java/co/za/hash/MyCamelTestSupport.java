package co.za.hash;

import static co.za.hash.util.LamdaUtil.entry;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.naming.Context;

import org.apache.camel.CamelContext;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.JndiRegistry;
import org.apache.camel.impl.PropertyPlaceholderDelegateRegistry;
import org.apache.camel.model.RouteDefinition;
import org.apache.camel.spi.Registry;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.util.jndi.JndiContext;
import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import co.za.hash.camel.MyRouteBuilder;
import co.za.hash.config.ApplicationProperties;
import co.za.hash.config.CamelConfig;
import co.za.hash.config.JacksonAppProp;
import co.za.hash.util.StringUtil;

public abstract class MyCamelTestSupport extends CamelTestSupport{
	final protected Map<String, List<Weave>> weaveIds;
	protected Map<String, List<String>> weaveIdsObs;
	protected List<RouteBuilder> routes;
	protected final String routeId;
	protected final List<Object> beans = new ArrayList<Object>();
	protected final ObjectMapper mapper = new ObjectMapper();
	protected static ApplicationProperties props;
	protected final String direct;
	protected Map<String, MockEndpoint> mocks;
	
	static {
		try{
			props = new ObjectMapper(new YAMLFactory()).readValue((new ClassPathResource("application.yml")).getFile(),JacksonAppProp.class).getProps();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	
	
	
//	public MyCamelTestSupport(RouteBuilder route, String routeId, String weaveIdsCsv, Object ...beans) throws JsonParseException, JsonMappingException, IOException {
//		super();
//		weaveIds = new HashMap<String, List<Weave>>();
//		this.routes = Arrays.asList(route);
//		this.routeId = routeId;
//		if(weaveIdsCsv != null)weaveIds.put(routeId,Stream.of(weaveIdsCsv.split(",")).map(Weave::new).collect(Collectors.toList()));
//		this.beans.addAll(Arrays.asList(beans));
//		this.direct = "direct:"+routeId;
//	}
//	
//	public MyCamelTestSupport(String routeId, List<String> weaveIds) {
//		this.weaveIds = new HashMap<String, List<Weave>>();
//		if(weaveIds != null)this.weaveIds.put(routeId, weaveIds.stream().map(Weave::new).collect(Collectors.toList()));
//		this.routeId = routeId;
//		this.direct = "direct:"+routeId;
//	}
	
	public MyCamelTestSupport(String routeId, Map<String, List<String>> weaveIds){
		this.weaveIds = new HashMap<String, List<Weave>>();
		this.routeId = routeId;
		this.direct = "direct:"+routeId;
		this.weaveIds.putAll(weaveIds.entrySet().stream()
				.map(e->entry(e.getKey(), e.getValue().stream().map(Weave::new).collect(Collectors.toList()))
				).collect(Collectors.toMap(Entry::getKey, Entry::getValue)));
		this.routes = initRoutes();
	}
	
	public MyCamelTestSupport(Map<String, List<Weave>> weaveIds, String routeId){
		this.weaveIds = new HashMap<String, List<Weave>>();
		this.routeId = routeId;
		this.direct = "direct:"+routeId;
		this.weaveIds.putAll(weaveIds);
		//this.routes = initRoutes();
	}
	
	protected abstract List<RouteBuilder> initRoutes();
	
			
	@Override
	protected RoutesBuilder[] createRouteBuilders() throws Exception {
        return (RoutesBuilder[]) routes.toArray();
    }
	
	@Override
	protected Context createJndiContext() throws Exception {
	    JndiContext context = new JndiContext();
	    for(Object b: beans){
	    	context.bind(StringUtil.firstCharLow(b.getClass().getSimpleName()), b);
	    }
	    return context;
	}

	@Override
	protected CamelContext createCamelContext() throws Exception {
		CamelContext context = super.createCamelContext();
		//context.addComponent("ibm-mq", JmsComponent.jmsComponent());
		//context.getTypeConverterRegistry().addFallbackTypeConverter(new ConstructorConverter(), true);
		Registry registry = context.getRegistry(); 
        if (registry instanceof PropertyPlaceholderDelegateRegistry) 
        	registry = ((PropertyPlaceholderDelegateRegistry)registry).getRegistry(); 
		JndiRegistry jndiRegistry = (JndiRegistry) registry; 
		jndiRegistry.bind("sslContextParameters", (new CamelConfig()).sslContextParameters());
		replaceRouteFromWith(routeId, direct);
		return context;
	}
	
	@Override
	protected void doPostSetup() throws Exception{
		mockEndpoints();
		mocks = weaveIds.values().stream().flatMap(v->v.stream())
				.map(i->getMockEndpoint("mock:"+i.getId()))
				.collect(Collectors.toMap(MockEndpoint::getName, Function.identity()));
	}
	
	
	public void mockEndpoints() throws Exception {
		weaveIds.entrySet().stream().forEach(e->{
			AdviceWithRouteBuilder mockRoute = new AdviceWithRouteBuilder() {
		        @Override
		        public void configure() throws Exception {
		        	e.getValue().stream().forEach(w->{
		        		if(w.isReplace())weaveById(w.getId()).replace().to("mock:"+w.getId());
		        		else weaveById(w.getId()).after().to("mock:"+w.getId());
		        	});
		        }
		    };
		    
		    RouteDefinition routeDefinition=null;
			
		    try {
				routeDefinition = context.getRouteDefinition(e.getKey());
				routeDefinition.adviceWith(context, mockRoute);
			} catch (Exception e1) {
				System.out.println(""+routeDefinition+" "+e);
				e1.printStackTrace();
			}
		});
		
		
	    
	}
	
	protected String getFileString(String path){
		return new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(path)))
		.lines().collect(Collectors.joining("\n"));
	}
	
	protected InputStream getFileStream(String path){
		return this.getClass().getResourceAsStream(path);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected Properties useOverridePropertiesWithPropertiesComponent() {
		YamlPropertySourceLoader loader = new YamlPropertySourceLoader();
		try {
			PropertySource<?> applicationYamlPropertySource = loader.load("properties",
					new ClassPathResource("application.yml"), null);
			Map<String, Object> source = ((MapPropertySource) applicationYamlPropertySource).getSource();
			Map sourceString = source.entrySet().stream().map(e -> {
				e.setValue(e.getValue().toString());
				return e;
			}).collect(Collectors.toMap(e -> e.getKey(), Entry::getValue));

			Properties properties = new Properties();
			properties.putAll(sourceString);
			return properties;
		} catch (IOException e) {
			System.err.println("Config file cannot be found.");
		}
		
		

		return null;
	}
	
//	protected ObjectMapper mapper() {
//	    return Jackson2ObjectMapperBuilder.json()
//	            //.featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS) //ISODate
//	            .modules(new JavaTimeModule())
//	            .build();
//	}
	
	
}
