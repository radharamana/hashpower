package co.za.hash;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.ExpressionBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.junit.Before;

import co.za.hash.config.ApplicationProperties.Algo;
import co.za.hash.domain.Market;
import co.za.hash.domain.Order;
import co.za.hash.domain.nice.NiceAuth;
import co.za.hash.routes.nice.NiceRoute;
import co.za.hash.service.MarketService;
import co.za.hash.service.MultiPoolService;
import co.za.hash.service.OrderService;
import co.za.hash.service.PoolService;
import co.za.hash.service.WalletService;

public abstract class HashTestSupport extends MyCamelTestSupport {
	
	protected WalletService walletService = new WalletService(props);
	protected MultiPoolService multiPoolService = new MultiPoolService(props);
	protected MarketService marketService = new MarketService(props, multiPoolService);
	
	protected PoolService poolService = new PoolService(props, marketService);
	protected OrderService orderService = new OrderService(props, poolService, walletService);
	protected NiceAuth niceAuth = new NiceAuth(props);
	protected InputStream successStd = getFileStream("nice_success_response.json");
	protected InputStream failureStd = getFileStream("nice_failure_response.json");
	
	protected String failureStdStr = getFileString("nice_failure_response.json");
	protected String successStdStr = getFileString("nice_success_response.json");
	
	protected InputStream successNewOrder = getFileStream("nice_success_new_order.json");
	protected InputStream successNewOrderBlank = getFileStream("nice_success_new_order_blank.json");
	

	public HashTestSupport(String routeId, Map<String, List<String>> weaveIds) {
		super(routeId, weaveIds);
		routes = initRoutes();
	}
	
	public HashTestSupport(Map<String, List<Weave>> weaveIds, String routeId) {
		super(weaveIds, routeId);
		routes = initRoutes();
	}

	protected MockEndpoint doResponseRest(String restId, InputStream resp){
		MockEndpoint mockRest = mocks.get(restId);
		mockRest.expectedMessageCount(1);
		mockRest.returnReplyBody(ExpressionBuilder.constantExpression(resp));
		return mockRest;
	}
	
	protected MockEndpoint doResponseRest(String restId, String resp){
		MockEndpoint mockRest = mocks.get(restId);
		mockRest.expectedMessageCount(1);
		mockRest.returnReplyBody(ExpressionBuilder.constantExpression(resp));
		return mockRest;
	}
	
	
	
	protected static String getRestHeader(MockEndpoint restEnd){
		if(restEnd.getReceivedExchanges().isEmpty())throw new ArrayIndexOutOfBoundsException("No exhanges recieved for "+restEnd.getName());
		return (String)restEnd.getReceivedExchanges().get(0).getIn().getHeader("rest");
	}
	
	protected String getSpeedLimitRest(Integer id){
		return String.format(NiceRoute.STR_SPEED_REST, props.getNice().getApiId(), props.getNice().getApiKey(), props.getDpools().get(0).getLocation(), props.getDpools().get(0).getAlgo(),id, props.getDpools().get(0).getLimitSpeed() ) ;
	}
	
	protected String getSpeedMinRest(Integer id, Algo algo){
		return String.format(NiceRoute.STR_SPEED_REST, props.getNice().getApiId(), props.getNice().getApiKey(), props.getDpools().get(0).getLocation(), props.getDpools().get(0).getAlgo(),id, algo.getMinSpeed() ) ;
	}
	
	protected String getPriceDecRest(Integer id) {
		return String.format(NiceRoute.STR_DEC_PRICE_REST, props.getNice().getApiId(), props.getNice().getApiKey(), props.getDpools().get(0).getLocation(), props.getDpools().get(0).getAlgo(),id ) ;
	}
	
	protected String getPriceIncRest(Integer id, Market market) {
		return String.format(NiceRoute.STR_INC_PRICE_REST, props.getNice().getApiId(), props.getNice().getApiKey(), props.getDpools().get(0).getLocation(), props.getDpools().get(0).getAlgo(),id, market.getMinPrice() ) ;
	}
	
	protected String getRefillRest(Integer id, BigDecimal amount) {
		return String.format(NiceRoute.STR_REFILL_REST, props.getNice().getApiId(), props.getNice().getApiKey(), props.getDpools().get(0).getLocation(), props.getDpools().get(0).getAlgo(),id, amount ) ;
	}
	protected String getNewOrderRest(Order order){
		return String.format(NiceRoute.STR_NEW_ORDER_REST,props.getNice().getApiId(), props.getNice().getApiKey(), order.getPool().getLoc(), order.getMarket().getAlgo().getId(),order.getMarket().getAlgo().getAmount(), order.getPrice(), order.getLimitSpeed()
				,order.getPool().getHost(), order.getPool().getPort(), order.getPool().getUser()+order.getMarket().getId(), order.getPool().getPass(), niceAuth.getKey()); 
	}
	//"https://api.nicehash.com/api?method=orders.create&id=51467&key=e5af66da-2e0d-d9a1-736d-163c698a6bb1"
	//+ "&location=0&algo=1&amount=0.05&price=0.0625&limit=3&pool_host=eu.stratum.slushpool.com&pool_port=3333&pool_user=radharamana.worker1&pool_pass=x";
	
	
	protected final class EmptyOrderProcessor implements Processor {
		public EmptyOrderProcessor(){}
		
		@Override
		public void process(Exchange exchange) throws Exception {
			exchange.getIn().setBody(getFileString("nice_my_orders_empty.json"));
		}
	}
	

	@Before
	public void setup() throws Exception{
		orderService.setOrders(new ArrayList<Order>());
		walletService.setBtcAvailable(new BigDecimal(1));
		marketService.get(0, 1).setMinPrice(new BigDecimal("0.0625"));
	}
	

}
