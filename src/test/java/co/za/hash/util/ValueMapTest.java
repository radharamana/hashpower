package co.za.hash.util;

import static org.junit.Assert.assertEquals;

import java.util.TreeMap;

import org.junit.Test;

import com.google.common.collect.Ordering;

import co.za.hash.config.ApplicationProperties.Algo;

public class ValueMapTest {
	@Test
	public void testMap(){
		TreeMap<Algo, Integer> map = new ValueComparableMap<Algo, Integer>(Ordering.natural());
        Algo a = new Algo(1), b= new Algo(2), c= new Algo(3), d= new Algo(4), e= new Algo(5);
        map.put(a, 5);
        map.put(b, 1);
        map.put(c, 3);
        assertEquals(b,map.firstKey());
        assertEquals(a,map.lastKey());
        map.put(d,0);
        assertEquals(d,map.firstKey());
        //ensure it's still a map (by overwriting a key, but with a new value) 
        map.put(d, 2);
        assertEquals(b, map.firstKey());
        //Ensure multiple values do not clobber keys
        map.put(e, 2);
        assertEquals(5, map.size());
        assertEquals(2, (int) map.get(e));
        assertEquals(2, (int) map.get(d));
        assertEquals(5, (int) map.get(a));
        assertEquals(1, (int) map.get(b));
        
	}
}
