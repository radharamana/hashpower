package co.za.hash;

public class Weave {
	private String id;
	private boolean isReplace = true;
	public Weave(String id, boolean isReplace) {
		super();
		this.id = id;
		this.isReplace = isReplace;
	}
	public Weave(String id) {
		super();
		this.id = id;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isReplace() {
		return isReplace;
	}
	public void setReplace(boolean isReplace) {
		this.isReplace = isReplace;
	}
	
	
}
